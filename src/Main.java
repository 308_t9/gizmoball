import java.io.IOException;

import model.Board;
import view.GuiFrame;
import view.threed.View3D;

import com.esotericsoftware.minlog.Log;

public class Main {
	public static void main(String[] args) {
		Log.TRACE();
		Board b = new Board();
		//View3D view3d = new View3D(b, b);
		new GuiFrame(b, b, b, b);
	}
}
