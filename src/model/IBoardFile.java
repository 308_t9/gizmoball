package model;

import java.io.IOException;

public interface IBoardFile {

	/**
	 * Write the current model to filepath.
	 * @throws IOException in the event that the operation is unsuccessful.
	 */
	public void save(String filepath) throws IOException;

	/**
	 * Load a saved model state from filepath.
	 * @throws IOException in the event that the operation is unsuccessful.
	 */
	public void load(String filepath) throws IOException;

}
