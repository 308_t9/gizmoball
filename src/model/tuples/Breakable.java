package model.tuples;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.List;
import java.util.Map;

import model.ColorPicker;
import model.gizmos.AbstractGizmo;
import model.gizmos.Ball;
import model.gizmos.Bumper;
import view.IViewDraw;

public class Breakable extends Bumper{

private double damage = 1;
private Bumper bump;
	
	public Breakable(Bumper b){
		super();
		this.bump = b;
		setColor(ColorPicker.BROWN);
	}
	
	@Override
	public void collidedWith(Ball b) {
		this.bump.collidedWith(b);
		damage -= 0.01*b.getVelocity().length();
		if(damage <= 0)
			model.deleteGizmo(bump.getX(), bump.getY());
	}
	
	@Override
	public void paintGizmo(IViewDraw v) {
		Color c = bump.getColor();
		bump.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), Math.max(120,(int)(255*damage))));
		bump.paintGizmo(v);
	}
	
	@Override
	public void passTime(double time) {
		bump.passTime(time);
	}

	public int getX() {
		return bump.getY();
	}

	public int getY() {
		return bump.getX();
	}

	@Override
	public int getWidth() {
		return bump.getWidth();
	}

	@Override
	public int getHeight() {
		return bump.getHeight();
	}
	
	public Color getColor() {
		return c;
	}

	@Override
	public boolean covers(double x, double y) {
		return bump.covers(x, y);
	}

	@Override
	public void keyDown() {
		bump.keyDown();
	}

	@Override
	public void keyUp() {
		bump.keyUp();
	}

	@Override
	public void paintEffects(IViewDraw v) {
		bump.paintEffects(v);
	}

	@Override
	public void move(double x, double y) {
		bump.move(x, y);
	}

	@Override
	public void setColor(Color c) {
		bump.setColor(c);
	}
	@Override
	public GizmoPhysics asPhysics() {
		return bump.asPhysics();
	}
	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		return bump.declaration(names);
	}

}
