package model.tuples;

import java.util.HashSet;
import java.util.Set;

import model.gizmos.AbstractGizmo;
import physics.Circle;
import physics.LineSegment;
import physics.Vect;

public class GizmoPhysics {

	public final Set<LineSegment> ls;
	public final Set<Circle> cs;
	public final Set<LineSegment> rls;
	public final Set<Circle> rcs;
	public final Vect cor;
	public final double angVelo;
	public final AbstractGizmo g;
	public final double ref;

	public GizmoPhysics(Set<LineSegment> ls, Set<Circle> cs,
			Set<LineSegment> rls, Set<Circle> rcs, AbstractGizmo g, double ref,
			Vect cor, double angVelo) {
		this.ls = (ls != null) ? ls : new HashSet<LineSegment>();
		this.cs = (cs != null) ? cs : new HashSet<Circle>();
		this.rls = (rls != null) ? rls : new HashSet<LineSegment>();
		this.rcs = (rcs != null) ? rcs : new HashSet<Circle>();
		this.g = g;
		this.ref = ref;
		this.cor = cor;
		if (cor == null) {
			this.ls.addAll(this.rls);
			this.cs.addAll(this.rcs);
			this.rls.removeAll(this.ls);
			this.rcs.removeAll(this.cs);
		}
		this.angVelo = angVelo;
	}

	public GizmoPhysics(Set<LineSegment> ls, Set<Circle> cs, AbstractGizmo g,
			double ref) {
		this(ls, cs, null, null, g, ref, null, 0);
	}

	public static Set<LineSegment> lsForRectangle(double x, double y, double w,
			double h) {
		double x2 = x + w;
		double y2 = y + h;

		Set<LineSegment> ls = new HashSet<>();
		ls.add(new LineSegment(x, y, x2, y));
		ls.add(new LineSegment(x2, y, x2, y2));
		ls.add(new LineSegment(x2, y2, x, y2));
		ls.add(new LineSegment(x, y2, x, y));
		return ls;
	}

	public static Set<Circle> csForRectangle(double x, double y, double w,
			double h) {
		double x2 = x + w;
		double y2 = y + h;

		Set<Circle> cs = new HashSet<>();
		cs.add(new Circle(x, y, 0));
		cs.add(new Circle(x2, y, 0));
		cs.add(new Circle(x2, y2, 0));
		cs.add(new Circle(x, y2, 0));
		return cs;
	}

}
