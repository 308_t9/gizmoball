package model.tuples;

import model.gizmos.Ball;
import physics.Vect;

public class TeleportDescriptor {
	public Vect normalisedVelocity;
	public double relativeDisplacement;
	public Ball ball;

	public TeleportDescriptor(Vect normalisedVelocity,
			double relativeDisplacement, Ball ball) {
		this.normalisedVelocity = normalisedVelocity;
		this.relativeDisplacement = relativeDisplacement;
		this.ball = ball;
	}
}