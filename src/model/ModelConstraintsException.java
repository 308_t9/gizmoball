package model;

public class ModelConstraintsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ModelConstraintsException(String msg) {
		super(msg);
	}

}
