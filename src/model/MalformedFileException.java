package model;

public class MalformedFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MalformedFileException(String msg) {
		super(msg);
	}

}
