package model;

public class IBoardConstants {

	public static final double MAX_GRAVITY = 50;
	public static final double MIN_GRAVITY = -MAX_GRAVITY;
	public static final double DEFAULT_GRAVITY = 25;
	public static final double MAX_MU = 0.05;
	public static final double MIN_MU = 0;
	public static final double DEFAULT_MU = 0.025;
	public static final double MAX_MU2 = 0.05;
	public static final double MIN_MU2 = 0;
	public static final double DEFAULT_MU2 = 0.025;
	public static final double MAX_BALL_SPEED = 200;
	public static final double MIN_BALL_SPEED = 0;
	public static final int DEFAULT_BALL_SPEED = 20;

	public static final double DEFAULT_WIDTH = 20;
	public static final double DEFAULT_HEIGHT = 20;

	public static double bounded(double max, double min, double val) {
		if (val < min)
			return min;
		if (val > max)
			return max;
		return val;
	}
}
