package model;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import model.gizmos.AbstractGizmo;

public class ColorPicker {


	public static final Color BROWN = new Color(128, 53, 0);
	private static List<Color> cs = Arrays.asList(new Color(0xf37e79),
			new Color(0x7998f3), new Color(0xbbf379), new Color(0xf379df),
			new Color(0x79f3e3), new Color(0xf3bf79), new Color(0x9c79f3),
			new Color(0x7af379), new Color(0xf3799d), new Color(0x79c1f3),
			new Color(0xe4f379), new Color(0xde79f3), new Color(0x79f3ba),
			new Color(0xf39779), new Color(0x797ff3), new Color(0xa2f379),
			new Color(0xf379c6), new Color(0x79e9f3), new Color(0xf3d979),
			new Color(0xb579f3), new Color(0x79f392), new Color(0xf37984),
			new Color(0x79a8f3), new Color(0xcbf379), new Color(0xf379ee),
			new Color(0x79f3d3), Color.RED, Color.CYAN, Color.YELLOW,
			Color.GREEN, BROWN);

	public static Color pick() {
		return cs.get(AbstractGizmo.rand.nextInt(cs.size()));
	}

}
