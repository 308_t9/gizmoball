package model.gizmos;

import java.awt.Color;

import model.ColorPicker;
import view.IViewDraw;
import view.RotationSpecifier;

public abstract class Bumper extends AbstractGizmo {

	protected Color c;
	protected Color smoke;
	protected double smokeDistance;
	protected double smokeFactor;
	protected double x, y, w, h;

	public Bumper() {
		w = 1;
		h = 1;
	}

	@Override
	public void passTime(double time) {
		if (smoke != null) {
			smoke = new Color(c.getRed(), c.getGreen(), c.getBlue(), Math.max(
					smoke.getAlpha() - 2, 0));
			smokeDistance += time;
		}
	}

	@Override
	public void collidedWith(Ball b) {
		super.collidedWith(b);
		if ((smoke == null || smoke.getAlpha() == 0)
				&& Math.abs(b.getVelocity().y()) > 2) {
			smokeFactor = 55 * (b.getVelocity().length() / 200);
			smoke = new Color(c.getRed(), c.getGreen(), c.getBlue(), Math.min(
					255, 200 + (int) smokeFactor));
			smokeDistance = 0;
		}

	}

	public int getX() {
		return (int) x;
	}

	public int getY() {
		return (int) y;
	}

	@Override
	public int getWidth() {
		return (int) w;
	}

	@Override
	public int getHeight() {
		return (int) h;
	}
	
	public Color getColor() {
		return c;
	}

	@Override
	public boolean covers(double x, double y) {
		double x2 = this.x + w;
		double y2 = this.y + h;
		return (this.x <= x && x < x2 && this.y <= y && y < y2);
	}

	@Override
	public void keyDown() {
		c = ColorPicker.pick();
	}

	@Override
	public void keyUp() {
		keyDown();
	}

	@Override
	public void paintEffects(IViewDraw v) {
		if (smoke != null) {
			double r = Math.min(smokeDistance + 0.05 * smokeFactor, 5);
			RotationSpecifier rotationSpecifier = new RotationSpecifier(
					Math.toRadians((smokeDistance + 0.1 * smokeFactor) * 10),
					x + 0.5, y + 0.5);
			v.drawSphere(smoke, x + 0.5 - r, y - r - smokeDistance, 2*r);
			v.drawSphere(smoke, x + 1 + r + smokeDistance, y + r, 2*r);
			v.drawSphere(smoke, x + 0.5 - r, y + 1 + r + smokeDistance, 2*r);
			v.drawSphere(smoke, x - r - smokeDistance, y + r, 2*r);

		}
	}

	@Override
	public void move(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
	}

}
