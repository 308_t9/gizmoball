package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import physics.Circle;
import physics.LineSegment;
import model.tuples.GizmoPhysics;
import view.IViewDraw;

public class Walls extends Bumper {

	private GizmoPhysics gp;

	public Walls(double width, double height) {
		Set<LineSegment> ls = new HashSet<>();
		Set<Circle> cs = new HashSet<>();
		w = (int) width;
		h = (int) height;
		ls.add(new LineSegment(0, 0, w, 0));
		ls.add(new LineSegment(w, 0, w, h));
		ls.add(new LineSegment(w, h, 0, h));
		ls.add(new LineSegment(0, h, 0, 0));
		cs.add(new Circle(0, 0, 0));
		cs.add(new Circle(w, 0, 0));
		cs.add(new Circle(0, h, 0));
		cs.add(new Circle(w, h, 0));
		gp = new GizmoPhysics(ls, cs, this, 1);
	}

	@Override
	public void paintGizmo(IViewDraw v) {
	}

	@Override
	public void collidedWith(Ball b) {
		if (down)
			for (AbstractGizmo g : connectedTo)
				g.keyDown();
		else
			for (AbstractGizmo g : connectedTo)
				g.keyUp();
		down = !down;
	}

	@Override
	public void passTime(double time) {
	}

	@Override
	public void keyDown() {
	}

	@Override
	public void keyUp() {
	}

	@Override
	public GizmoPhysics asPhysics() {
		return gp;
	}

	@Override
	public void move(double x, double y) {
	}

	@Override
	public void rotate() {
	}

	@Override
	public void setColor(Color c) {
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		return Arrays.asList("");
	}

	@Override
	public boolean covers(double x, double y) {
		return false;
	}

}
