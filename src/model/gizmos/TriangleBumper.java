package model.gizmos;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.tuples.GizmoPhysics;
import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;
import view.IViewDraw;
import view.RotationSpecifier;

public class TriangleBumper extends Bumper {

	public TriangleBumper(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		a = RotationAngle.ZERO;
		c = Color.CYAN;
	}

	public TriangleBumper(double x, double y, double w, double h) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		this.w = Math.min(Math.floor(w), Math.floor(h));
		this.h = Math.min(Math.floor(w), Math.floor(h));
		a = RotationAngle.ZERO;
		this.c = Color.CYAN;
	}
	@Override
	public void paintGizmo(IViewDraw v) {
		v.drawPolygon(c, x, y, w, h, Arrays.asList(new RotationSpecifier(a.getTheta(),
				x + w/2, y + h/2)), new Point2D.Double(x, y),
				new Point2D.Double(x + w, y), new Point2D.Double(x, y + h));
	}

	@Override
	public GizmoPhysics asPhysics() {
		Set<LineSegment> ls = new HashSet<>();
		Set<Circle> cs = new HashSet<>();

		LineSegment l1 = new LineSegment(x, y, x + w, y);
		LineSegment l2 = new LineSegment(x + w, y, x, y + h);
		LineSegment l3 = new LineSegment(x, y + h, x, y);

		Circle c1 = new Circle(x, y, 0);
		Circle c2 = new Circle(x + w, y, 0);
		Circle c3 = new Circle(x, y + h, 0);

		Vect cor = new Vect(x + w/2, y + h/2);
		Angle a = new Angle(this.a.getTheta());

		ls.add(Geometry.rotateAround(l1, cor, a));
		ls.add(Geometry.rotateAround(l2, cor, a));
		ls.add(Geometry.rotateAround(l3, cor, a));

		cs.add(Geometry.rotateAround(c1, cor, a));
		cs.add(Geometry.rotateAround(c2, cor, a));
		cs.add(Geometry.rotateAround(c3, cor, a));

		return new GizmoPhysics(ls, cs, this, 1.0);
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException(ErrorStrings.NAME_WAS_NULL);
		List<String> ret = new ArrayList<>();
		ret.add(String.format("Triangle %s %d %d", name, (int) x, (int) y));

		String rotate = String.format("Rotate %s", name);

		switch (a) {
		case TWO_SEVENTY:
			ret.add(rotate);
		case ONE_EIGHTY:
			ret.add(rotate);
		case NINETY:
			ret.add(rotate);
		default:
			break;
		}

		return ret;
	}

}
