package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import physics.Circle;
import physics.LineSegment;
import model.tuples.GizmoPhysics;
import view.IViewDraw;

public class CircleBumper extends Bumper {

	public CircleBumper(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		c = Color.YELLOW;
	}
	
	public CircleBumper(double x, double y, double w, double h) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		c = Color.YELLOW;
		this.w = Math.floor(Math.min(w, h));
		this.h = Math.floor(Math.min(w, h));
	}

	@Override
	public void paintGizmo(IViewDraw v) {
		v.drawEllipse(c, x, y, w, h);
	}

	@Override
	public GizmoPhysics asPhysics() {
		Set<Circle> cs = new HashSet<>();
		cs.add(new Circle(x + w/2, y + h/2, w/2));

		return new GizmoPhysics(new HashSet<LineSegment>(), cs, this, 1.0);
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException("Name was null.");
		return Arrays.asList(String.format("Circle %s %d %d", name, (int) x,
				(int) y));
	}

}
