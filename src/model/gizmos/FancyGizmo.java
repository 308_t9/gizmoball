package model.gizmos;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.tuples.GizmoPhysics;
import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;
import view.IViewDraw;
import view.RotationSpecifier;

public class FancyGizmo extends AbstractGizmo {

	private Color c;
	private double x, y, w, h;
	private static final double r = 0.25;
	private double theta;
	private static final double angVelo = Math.toRadians(545);

	public FancyGizmo(double x, double y) {
		c = Color.ORANGE;
		this.x = x;
		this.y = y;
		w = 2;
		h = 2;
		theta = 0;
	}

	@Override
	public void paintGizmo(IViewDraw v) {
		List<RotationSpecifier> rot = new ArrayList<>();
		rot.add(new RotationSpecifier(theta, x + 1, y + 1));

		Point2D.Double p1 = new Point2D.Double(x + 1, y);
		Point2D.Double p2 = new Point2D.Double(x + 1 + r, y + r);
		Point2D.Double p3 = new Point2D.Double(x + 1 + r, y + 1 - r);
		Point2D.Double p4 = new Point2D.Double(x + 2 - r, y + 1 - r);
		Point2D.Double p5 = new Point2D.Double(x + 2, y + 1);
		Point2D.Double p6 = new Point2D.Double(x + 2 - r, y + 1 + r);
		Point2D.Double p7 = new Point2D.Double(x + 1 + r, y + 1 + r);
		Point2D.Double p8 = new Point2D.Double(x + 1 + r, y + 2 - r);
		Point2D.Double p9 = new Point2D.Double(x + 1, y + 2);
		Point2D.Double p10 = new Point2D.Double(x + 1 - r, y + 2 - r);
		Point2D.Double p11 = new Point2D.Double(x + 1 - r, y + 1 + r);
		Point2D.Double p12 = new Point2D.Double(x + r, y + 1 + r);
		Point2D.Double p13 = new Point2D.Double(x, y + 1);
		Point2D.Double p14 = new Point2D.Double(x + r, y + 1 - r);
		Point2D.Double p15 = new Point2D.Double(x + 1 - r, y + 1 - r);
		Point2D.Double p16 = new Point2D.Double(x + 1 - r, y + r);

		v.drawPolygon(c, x, y, w, h, rot, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11,
				p12, p13, p14, p15, p16);
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public int getWidth() {
		return (int) w;
	}

	@Override
	public int getHeight() {
		return (int) h;
	}

	@Override
	public void passTime(double time) {
		theta += time * angVelo;
		theta %= 2 * Math.PI;
	}

	@Override
	public void move(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
	}

	@Override
	public GizmoPhysics asPhysics() {

		Set<LineSegment> templ = new HashSet<>();
		Set<Circle> tempc = new HashSet<>();

		Set<LineSegment> rls = new HashSet<>();
		Set<Circle> rcs = new HashSet<>();

		tempc.add(new Circle(x + 1, y, 0));
		tempc.add(new Circle(x + 2, y + 1, 0));
		tempc.add(new Circle(x + 1, y + 2, 0));
		tempc.add(new Circle(x, y + 1, 0));

		templ.add(new LineSegment(x + 1 - r, y + r, x + 1, y));
		templ.add(new LineSegment(x + 1 + r, y + r, x + 1, y));
		templ.add(new LineSegment(x + r, y + 1 - r, x, y + 1));
		templ.add(new LineSegment(x + r, y + 1 + r, x, y + 1));

		templ.add(new LineSegment(x + 1 - r, y + 2 - r, x + 1, y + 2));
		templ.add(new LineSegment(x + 1 + r, y + 2 - r, x + 1, y + 2));
		templ.add(new LineSegment(x + 2 - r, y + 1 - r, x + 2, y + 1));
		templ.add(new LineSegment(x + 2 - r, y + 1 + r, x + 2, y + 1));

		templ.add(new LineSegment(x + 1 - r, y + r, x + 1 - r, y + 1 - r));
		templ.add(new LineSegment(x + 1 + r, y + r, x + 1 + r, y + 1 - r));
		templ.add(new LineSegment(x + r, y + 1 - r, x + 1 - r, y + 1 - r));
		templ.add(new LineSegment(x + r, y + 1 + r, x + 1 - r, y + 1 + r));

		templ.add(new LineSegment(x + 1 - r, y + 1 + r, x + 1 - r, y + 2 - r));
		templ.add(new LineSegment(x + 1 + r, y + 1 + r, x + 1 + r, y + 2 - r));
		templ.add(new LineSegment(x + 1 + r, y + 1 - r, x + 2 - r, y + 1 - r));
		templ.add(new LineSegment(x + 1 + r, y + 1 + r, x + 2 - r, y + 1 + r));

		tempc.add(new Circle(x + 1 - r, y + r, 0));
		tempc.add(new Circle(x + 1 + r, y + r, 0));
		tempc.add(new Circle(x + 2 - r, y + 1 - r, 0));
		tempc.add(new Circle(x + 2 - r, y + 1 + r, 0));
		tempc.add(new Circle(x + 1 + r, y + 2 - r, 0));
		tempc.add(new Circle(x + 1 - r, y + 2 - r, 0));
		tempc.add(new Circle(x + r, y + 1 + r, 0));
		tempc.add(new Circle(x + r, y + 1 - r, 0));
		tempc.add(new Circle(x + 1 - r, y + 1 - r, 0));
		tempc.add(new Circle(x + 1 + r, y + 1 - r, 0));
		tempc.add(new Circle(x + 1 - r, y + 1 + r, 0));
		tempc.add(new Circle(x + 1 + r, y + 1 + r, 0));

		Angle a = new Angle(Math.toRadians(theta));
		Vect cor = new Vect(x + 1, y + 1);
		for (Circle circle : tempc) {
			rcs.add(Geometry.rotateAround(circle, cor, a));
		}

		for (LineSegment l : templ) {
			rls.add(Geometry.rotateAround(l, cor, a));
		}
		return new GizmoPhysics(null, null, rls, rcs, this, 1, cor, angVelo);
	}

	@Override
	public boolean covers(double x, double y) {
		double x2 = this.x + 2;
		double y2 = this.y + 2;

		return (this.x <= x && x < x2 && this.y <= y && y < y2);
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		return new ArrayList<>();
	}

}
