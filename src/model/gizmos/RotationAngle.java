package model.gizmos;

/**
 * @author team enum to extract rotation angle for rendering.
 */
public enum RotationAngle {

	ZERO(0), NINETY(Math.PI / (double) 2), ONE_EIGHTY(Math.PI), TWO_SEVENTY(
			Math.PI * (3.0 / 2.0));

	private final double theta;

	private RotationAngle(double theta) {
		this.theta = theta;
	}

	public double getTheta() {
		return theta;
	}
}
