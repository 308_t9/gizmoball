package model.gizmos;

import java.awt.Color;

import view.IViewDraw;

public interface IGizmo {

	public void paintGizmo(IViewDraw v);

	public void setColor(Color c);

	public void paintEffects(IViewDraw v);
}
