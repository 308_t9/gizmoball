package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.tuples.GizmoPhysics;
import model.tuples.TeleportDescriptor;
import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;
import view.IViewDraw;
import view.RotationSpecifier;

public class Portal extends AbstractGizmo {

	private double x, y;
	private PortalType t;
	private List<AbstractGizmo> portals;
	private static final double arcw = 0.25;
	private static final double arch = 0.5;
	private static final double w = 0.25;
	private static final double h = 2;
	private static final double r = 0.5 * w;

	private Color c;

	public Portal(double x, double y, PortalType t) {
		this.x = x;
		this.y = y;
		this.t = t;
		this.c = t.getColor();
		a = t == PortalType.BLUE ? RotationAngle.ZERO
				: RotationAngle.ONE_EIGHTY;
		portals = new ArrayList<>();
	}

	@Override
	public void paintGizmo(IViewDraw v) {

		double x = this.x;
		v.drawRoundedRectangle(c, x, y, w, h, arcw, arch,
				new RotationSpecifier(a.getTheta(), this.x + 1, this.y + 1));

	}

	@Override
	public void connect(AbstractGizmo g) {
		if (g instanceof Portal && !(g == this)) {
			if (!portals.contains(g))
				portals.add(g);
		} else {
			super.connect(g);
		}
	}

	@Override
	public void disconnect(AbstractGizmo g) {
		if (g instanceof Portal)
			portals.remove(g);
		else
			super.disconnect(g);
	}

	public PortalType getType() {
		return t;
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public int getWidth() {
		return 2;
	}

	@Override
	public int getHeight() {
		return 2;
	}

	@Override
	public void passTime(double time) {
		while (BallTeleportationService.getBTS().teleportExists(this)) {
			TeleportDescriptor d = BallTeleportationService.getBTS()
					.getTeleport(this);
			double theta = Math.toDegrees(d.normalisedVelocity.angle()
					.radians());
			double bx = 0;
			double by = 0;
			if (theta > -90 && theta < 90) {
				switch (a) {
				case ZERO:
					bx = x + w + Ball.r;
					by = y + d.relativeDisplacement;
					break;
				case NINETY:
					bx = x + d.relativeDisplacement;
					by = y + w + Ball.r;
					break;
				case ONE_EIGHTY:
					bx = x + 2 - w - Ball.r;
					by = y + d.relativeDisplacement;
					break;
				case TWO_SEVENTY:
					bx = x + d.relativeDisplacement;
					by = y + 2 - w - 2*Ball.r;
					break;
				}
			} else {
				switch (a) {
				case ZERO:
					bx = x - Ball.r;
					by = y + d.relativeDisplacement;
					break;
				case NINETY:
					bx = x + d.relativeDisplacement;
					by = y - Ball.r;
					break;
				case ONE_EIGHTY:
					bx = x + 2 + Ball.r;
					by = y + d.relativeDisplacement;
					break;
				case TWO_SEVENTY:
					bx = x + d.relativeDisplacement;
					by = y + 2 + Ball.r;
					break;
				}
			}
			if((theta > -90 && theta < 90) || model.squareFree(bx, by))
				if (!(bx < 0 + Ball.r || bx > 20-2*Ball.r || by < 0 || by > 20 - 2*Ball.r)) {
					d.ball.move(bx, by);
					d.ball.setVelocity(d.normalisedVelocity.rotateBy(new Angle(a
						.getTheta())));
				}
			}
	}

	@Override
	public void collidedWith(Ball b) {
		AbstractGizmo g = randomTarget();
		if (g != null) {
			Vect v = new Vect(b.getVelocity().angle(), b.getVelocity().length())
					.rotateBy(new Angle(-a.getTheta()));
			double relativeDisplacement;
			if (a == RotationAngle.ZERO || a == RotationAngle.ONE_EIGHTY)
				relativeDisplacement = b.getY() - y;
			else
				relativeDisplacement = b.getX() - x;
			BallTeleportationService.getBTS().registerTelport(g, v,
					relativeDisplacement, b);
		}
	}

	private AbstractGizmo randomTarget() {
		if (portals.size() > 0)
			return portals.get(AbstractGizmo.rand.nextInt(portals.size()));
		return null;
	}

	@Override
	public void move(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
	}

	@Override
	public boolean covers(double x, double y) {
		double x2 = this.x + 2;
		double y2 = this.y + 2;

		return (this.x <= x && x < x2 && this.y <= y && y < y2);

	}

	@Override
	public GizmoPhysics asPhysics() {
		Set<LineSegment> ls = new HashSet<>();
		Set<Circle> cs = new HashSet<>();

		double x = this.x;

		Circle top = new Circle(x + r, y + r, r);
		Circle bottom = new Circle(x + r, y + h - r, r);

		LineSegment l1 = new LineSegment(x, y + r, x, y + h - r);
		LineSegment l2 = new LineSegment(x + w, y + r, x + w, y + h - r);
		Circle c1 = new Circle(x, y + r, 0);
		Circle c2 = new Circle(x, y + h - r, 0);
		Circle c3 = new Circle(x + w, y + r, 0);
		Circle c4 = new Circle(x + w, y + h - r, 0);

		// Natural Rotation
		Vect center = new Vect(this.x + 1, y + 1);
		Angle natural = new Angle(a.getTheta());
		top = Geometry.rotateAround(top, center, natural);
		bottom = Geometry.rotateAround(bottom, center, natural);
		c1 = Geometry.rotateAround(c1, center, natural);
		c2 = Geometry.rotateAround(c2, center, natural);
		c3 = Geometry.rotateAround(c3, center, natural);
		c4 = Geometry.rotateAround(c4, center, natural);
		l1 = Geometry.rotateAround(l1, center, natural);
		l2 = Geometry.rotateAround(l2, center, natural);

		ls.add(l1);
		ls.add(l2);

		cs.add(top);
		cs.add(bottom);

		cs.add(c1);
		cs.add(c2);
		cs.add(c3);
		cs.add(c4);

		return new GizmoPhysics(ls, cs, this, 1);
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		return new ArrayList<>();
	}

}
