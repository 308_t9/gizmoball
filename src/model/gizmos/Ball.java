package model.gizmos;

import java.awt.Color;

import physics.Circle;
import physics.Vect;
import view.IViewDraw;

public class Ball implements IGizmo {

	private double x;
	private double y;
	private Vect velocity;
	public static final double r = 0.25;
	private boolean stopped;

	private Color c;

	public Ball(double x, double y, Vect velocity) {
		this.x = x;
		this.y = y;
		this.velocity = velocity;
		this.c = Color.BLUE;
	}

	public boolean isStopped() {
		return stopped;
	}
	

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}

	public void setVelocity(Vect velocity) {
		this.velocity = velocity;
	}

	public Vect getVelocity() {
		return this.velocity;
	}

	@Override
	public void paintGizmo(IViewDraw v) {
		v.drawSphere(c, x, y, r*2);
	}

	public Circle asCircle() {
		return new Circle(x + r, y + r, r);
	}

	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vect asVect() {
		return new Vect(x + r, y + r);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public void paintEffects(IViewDraw v) {
		// TODO Auto-generated method stub

	}
}
