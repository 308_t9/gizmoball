package model.gizmos;

import java.awt.Color;

public enum PortalType {

	BLUE(new Color(0, 106, 255)), ORANGE(new Color(255, 149, 0));

	private final Color c;

	private PortalType(Color c) {
		this.c = c;
	}

	public Color getColor() {
		return c;
	}

}
