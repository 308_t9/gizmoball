package model.gizmos;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import model.tuples.TeleportDescriptor;

import physics.Vect;

public class BallTeleportationService {

	private Map<AbstractGizmo, List<TeleportDescriptor>> register;

	// Private constructor - singleton.
	private BallTeleportationService() {
		register = new HashMap<>();
	}

	public void registerTelport(AbstractGizmo target, Vect normalisedVelocity,
			double relativeDisplacement, Ball ball) {
		getList(target).add(
				new TeleportDescriptor(normalisedVelocity,
						relativeDisplacement, ball));
	}

	private List<TeleportDescriptor> getList(AbstractGizmo target) {
		List<TeleportDescriptor> ret = register.get(target);
		if (ret == null)
			register.put(target, (ret = new LinkedList<TeleportDescriptor>()));
		return ret;
	}

	public boolean teleportExists(AbstractGizmo target) {
		return getList(target).size() > 0;
	}

	public TeleportDescriptor getTeleport(AbstractGizmo target) {
		List<TeleportDescriptor> list = getList(target);
		if (list.size() > 0)
			return list.remove(0);
		return null;
	}

	public static BallTeleportationService getBTS() {
		if (ref == null)
			ref = new BallTeleportationService();
		return ref;
	}

	private static BallTeleportationService ref;

}
