package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import model.IBoardConstants;
import model.tuples.GizmoPhysics;
import physics.Angle;
import physics.Vect;
import view.IViewDraw;

public class Absorber extends AbstractGizmo {

	private double x, y, w, h;
	private Ball ball;

	private Color c;

	public Absorber(double x, double y, double w, double h) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		this.w = Math.floor(w);
		this.h = Math.floor(h);
		this.c = Color.MAGENTA;
	}

	@Override
	public void paintGizmo(IViewDraw v) {
		v.drawRectangle(c, x, y, w, h);
	}

	@Override
	public void collidedWith(Ball b) {
		super.collidedWith(b);
		
		ball = b;
		ball.setStopped(true);
		if (y + h - 1 < IBoardConstants.DEFAULT_HEIGHT / 2) {
			ball.move(x + 0.01, y + h - 2 * Ball.r);
		} else
			ball.move(x + w - 2 * Ball.r - 0.01, y);
		if(connectedTo.contains(this))
			releaseBall();
	}

	@Override
	public void passTime(double time) {

	}

	@Override
	public GizmoPhysics asPhysics() {
		return new GizmoPhysics(GizmoPhysics.lsForRectangle(x, y, w, h),
				GizmoPhysics.csForRectangle(x, y, w, h), this, 0);
	}

	@Override
	public void move(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		if(ball != null) {
			ball.setStopped(false);
			ball = null;
		}
	}

	@Override
	public void rotate() {

	}

	@Override
	public boolean covers(double x, double y) {
		double x2 = this.x + w;
		double y2 = this.y + h;
		return this.x <= x && x < x2 && this.y <= y && y < y2;
	}

	@Override
	public void keyUp() {
		super.keyUp();
		releaseBall();
	}

	@Override
	public void keyDown() {
		super.keyDown();
		releaseBall();
	}

	private void releaseBall() {
		if (ball != null) {
			if (y + h - 1 < IBoardConstants.DEFAULT_HEIGHT / 2) {
				ball.move(ball.getX(), ball.getY() + 2 * Ball.r);
				ball.setVelocity(new Vect(Angle.DEG_90, 50));
			} else {
				ball.move(ball.getX(), ball.getY() - 2 * Ball.r);
				ball.setVelocity(new Vect(Angle.DEG_270, 50));
			}
			ball.setStopped(false);
			ball = null;
		}
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException(ErrorStrings.NAME_WAS_NULL);
		String d = String.format("Absorber %s %d %d %d %d", names.get(this),
				(int) x, (int) y, (int) (x + w), (int) (y + h));
		return Arrays.asList(d);
	}

	@Override
	public int getWidth() {
		return (int) w;
	}

	@Override
	public int getHeight() {
		return (int) h;
	}

}
