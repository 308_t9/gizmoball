package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.tuples.GizmoPhysics;
import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;
import view.IViewDraw;
import view.RotationSpecifier;

public class Flipper extends AbstractGizmo {

	private double x, y, theta;
	private FlipperDirection d;
	private static final double arcw = 0.5;
	private static final double arch = 0.5;
	private static final double w = 0.5;
	private static final double h = 2;
	private static final double r = 0.5 * w;

	private Color c;

	public Flipper(double x, double y, FlipperDirection d) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		this.d = d;
		theta = 0;
		c = Color.GREEN;
	}

	@Override
	public void paintGizmo(IViewDraw v) {

		double x = this.x;

		double thetaNatural = a.getTheta();
		double thetaInternal = -theta;
		if (d == FlipperDirection.RIGHT) {
			x += 1.5;
			thetaInternal = -thetaInternal;
		}

		double tx = x + r;
		double ty = y + r;

		v.drawRoundedRectangle(c, x, y, w, h, arcw, arch,
				new RotationSpecifier(thetaInternal, tx, ty),
				new RotationSpecifier(thetaNatural, this.x + 1, this.y + 1));
	}

	@Override
	public void collidedWith(Ball b) {
		super.collidedWith(b);
	}

	@Override
	public void passTime(double time) {
		if (keyDown && theta < Math.toRadians(90)) {
			theta = Math.min(Math.toRadians(90), 6 * Math.PI * time + theta);
		} else if (!keyDown && theta > 0) {
			theta = Math.max(0, theta - 6 * Math.PI * time);
		}
	}

	@Override
	public GizmoPhysics asPhysics() {
		Set<LineSegment> ls = new HashSet<>();
		Set<Circle> cs = new HashSet<>();

		double x = this.x;
		double theta = -this.theta;

		if (d == FlipperDirection.RIGHT) {
			theta = -theta;
			x += 1.5;
		}

		Circle top = new Circle(x + r, y + r, r);
		Circle bottom = new Circle(x + r, y + h - r, r);

		LineSegment l1 = new LineSegment(x, y + r, x, y + h - r);
		LineSegment l2 = new LineSegment(x + w, y + r, x + w, y + h - r);
		Circle c1 = new Circle(x, y + r, 0);
		Circle c2 = new Circle(x, y + h - r, 0);
		Circle c3 = new Circle(x + w, y + r, 0);
		Circle c4 = new Circle(x + w, y + h - r, 0);

		// Internal Rotation
		Angle internal = new Angle(theta);
		bottom = Geometry.rotateAround(bottom, top.getCenter(), internal);
		c1 = Geometry.rotateAround(c1, top.getCenter(), internal);
		c2 = Geometry.rotateAround(c2, top.getCenter(), internal);
		c3 = Geometry.rotateAround(c3, top.getCenter(), internal);
		c4 = Geometry.rotateAround(c4, top.getCenter(), internal);
		l1 = Geometry.rotateAround(l1, top.getCenter(), internal);
		l2 = Geometry.rotateAround(l2, top.getCenter(), internal);

		// Natural Rotation
		Vect center = new Vect(this.x + 1, y + 1);
		Angle natural = new Angle(a.getTheta());
		top = Geometry.rotateAround(top, center, natural);
		bottom = Geometry.rotateAround(bottom, center, natural);
		c1 = Geometry.rotateAround(c1, center, natural);
		c2 = Geometry.rotateAround(c2, center, natural);
		c3 = Geometry.rotateAround(c3, center, natural);
		c4 = Geometry.rotateAround(c4, center, natural);
		l1 = Geometry.rotateAround(l1, center, natural);
		l2 = Geometry.rotateAround(l2, center, natural);

		ls.add(l1);
		ls.add(l2);

		cs.add(top);
		cs.add(bottom);

		cs.add(c1);
		cs.add(c2);
		cs.add(c3);
		cs.add(c4);

		double angVelo;
		if (FlipperDirection.RIGHT == d)
			angVelo = 6 * Math.PI;
		else
			angVelo = -6 * Math.PI;

		if (keyDown && this.theta < Math.toRadians(90)) {
			return new GizmoPhysics(null, null, ls, cs, this, 0.95,
					top.getCenter(), angVelo);
		} else if (!keyDown && this.theta > 0) {
			return new GizmoPhysics(null, null, ls, cs, this, 0.95,
					top.getCenter(), -angVelo);
		} else
			return new GizmoPhysics(ls, cs, this, 0.95);
	}

	@Override
	public void move(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
	}

	@Override
	public boolean covers(double x, double y) {
		double x2 = this.x + 2;
		double y2 = this.y + 2;

		return (this.x <= x && x < x2 && this.y <= y && y < y2);

	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException("Name was null.");
		List<String> ret = new ArrayList<>();
		ret.add(String.format("%s %s %d %d",
				(d == FlipperDirection.LEFT) ? "LeftFlipper" : "RightFlipper",
				name, (int) x, (int) y));

		String rotate = String.format("Rotate %s", name);

		switch (a) {
		case TWO_SEVENTY:
			ret.add(rotate);
		case ONE_EIGHTY:
			ret.add(rotate);
		case NINETY:
			ret.add(rotate);
		case ZERO:
		}

		return ret;
	}

	@Override
	public int getWidth() {
		return 2;
	}

	@Override
	public int getHeight() {
		return 2;
	}
}
