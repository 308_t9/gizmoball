package model.gizmos;

import java.util.LinkedHashSet;
import java.util.Set;

import model.tuples.Breakable;

public class Gizmos {

	public static final String ABSORBER = "Absorber";
	public static final String ORANGE_PORTAL = "Orange Portal";
	public static final String BLUE_PORTAL = "Blue Portal";
	public static final String FANCY_GIZMO = "Fancy Gizmo";
	public static final String TRIANGLE_BUMPER = "Triangle Bumper";
	public static final String SQUARE_BUMPER = "Square Bumper";
	public static final String CIRCLE_BUMPER = "Circle Bumper";
	public static final String FLIPPER_LEFT = "Left Flipper";
	public static final String FLIPPER_RIGHT = "Right Flipper";
	public static final String STRETCHY_SQUARE = "Stretchy Square";
	public static final String STRETCHY_CIRCLE = "Stretchy Circle";
	public static final String STRETCHY_TRIANGLE = "Stretchy Triangle";
	public static final String BREAKABLE_SQUARE = "Breakable Square";
	public static final String BREAKABLE_TRIANGLE = "Breakable Triangle";
	public static final String BREAKABLE_CIRCLE = "Breakable Circle";
	public static final String STRETCHY_BREAKABLE_SQUARE = "Stretchy Breakable Square";
	public static final String STRETCHY_BREAKABLE_TRIANGLE = "Stretchy Breakable Triangle";
	public static final String STRETCHY_BREAKABLE_CIRCLE = "Stretchy Breakable Circle";

	Set<GizmoType> fixedTypes = new LinkedHashSet<>();
	Set<GizmoType> elasticTypes = new LinkedHashSet<>();

	public Gizmos() {
		fixedTypes.add(new GizmoType(Flipper.class, FLIPPER_LEFT));
		fixedTypes.add(new GizmoType(Flipper.class, FLIPPER_RIGHT));
		fixedTypes.add(new GizmoType(CircleBumper.class, CIRCLE_BUMPER));
		fixedTypes.add(new GizmoType(SquareBumper.class, SQUARE_BUMPER));
		fixedTypes.add(new GizmoType(TriangleBumper.class, TRIANGLE_BUMPER));
		fixedTypes.add(new GizmoType(FancyGizmo.class, FANCY_GIZMO));
		fixedTypes.add(new GizmoType(Portal.class, BLUE_PORTAL));
		fixedTypes.add(new GizmoType(Portal.class, ORANGE_PORTAL));
		fixedTypes.add(new GizmoType(Breakable.class, BREAKABLE_SQUARE));
		fixedTypes.add(new GizmoType(Breakable.class, BREAKABLE_TRIANGLE));
		fixedTypes.add(new GizmoType(Breakable.class, BREAKABLE_CIRCLE));

		elasticTypes.add(new GizmoType(Absorber.class, ABSORBER));
		elasticTypes.add(new GizmoType(SquareBumper.class, STRETCHY_SQUARE));
		elasticTypes.add(new GizmoType(CircleBumper.class, STRETCHY_CIRCLE));
		elasticTypes.add(new GizmoType(TriangleBumper.class, STRETCHY_TRIANGLE));
		elasticTypes.add(new GizmoType(Breakable.class, STRETCHY_BREAKABLE_SQUARE));
		elasticTypes.add(new GizmoType(Breakable.class, STRETCHY_BREAKABLE_TRIANGLE));
		elasticTypes.add(new GizmoType(Breakable.class, STRETCHY_BREAKABLE_CIRCLE));
	}

	public Set<GizmoType> getFixedTypes() {
		return fixedTypes;
	}

	public Set<GizmoType> getElasticTypes() {
		return elasticTypes;
	}
}
