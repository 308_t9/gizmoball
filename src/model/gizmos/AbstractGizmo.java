package model.gizmos;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import view.IViewDraw;
import model.Model;
import model.tuples.GizmoPhysics;

public abstract class AbstractGizmo implements IGizmo {

	protected Set<AbstractGizmo> connectedTo;
	protected boolean down;
	protected boolean keyDown;
	protected RotationAngle a;
	public static Random rand = new Random();
	protected static Model model;
	
	public static void setModel(Model model){
		AbstractGizmo.model = model;
	}
	
	public AbstractGizmo() {
		connectedTo = new HashSet<>();
		down = false;
		a = RotationAngle.ZERO;
	}

	public void collidedWith(Ball b) {
		if (down)
			for (AbstractGizmo g : connectedTo)
				g.keyDown();
		else
			for (AbstractGizmo g : connectedTo)
				g.keyUp();
		down = !down;

	}

	public abstract int getWidth();

	public abstract int getHeight();

	public abstract void passTime(double time);

	public void keyDown() {
		keyDown = true;
	}

	public void keyUp() {
		keyDown = false;
	}

	public abstract void move(double x, double y);

	public void rotate() {
		switch (a) {
		case ZERO:
			a = RotationAngle.NINETY;
			break;
		case NINETY:
			a = RotationAngle.ONE_EIGHTY;
			break;
		case ONE_EIGHTY:
			a = RotationAngle.TWO_SEVENTY;
			break;
		case TWO_SEVENTY:
			a = RotationAngle.ZERO;
			break;
		}
	}

	public abstract GizmoPhysics asPhysics();

	public void connect(AbstractGizmo g) {
		connectedTo.add(g);
	}

	public void disconnect(AbstractGizmo g) {
		connectedTo.remove(g);
	}

	public abstract boolean covers(double x, double y);

	public abstract List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException;

	public List<String> connections(Map<AbstractGizmo, String> names, Map<Integer, Set<AbstractGizmo>> triggersUp, Map<Integer, Set<AbstractGizmo>> triggersDown)
			throws UnexpectedException {
		List<String> cs = new ArrayList<>();
		List<String> ret = new ArrayList<>();
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException(ErrorStrings.NAME_WAS_NULL);

		for (AbstractGizmo g : connectedTo) {
			String gname = names.get(g);
			if (gname == null)
				throw new UnexpectedException(ErrorStrings.NAME_WAS_NULL);
			cs.add(gname);
		}
		
		for (String c : cs) {
			ret.add(String.format("Connect %s %s", name, c));
		}

		for(Integer i : triggersUp.keySet()){
			Set<AbstractGizmo> t = triggersUp.get(i);
			if(t != null)
				for(AbstractGizmo g : t)
					if(g == this){
						ret.add(String.format("KeyConnect key %s up %s", i, name));
						break;
					}
		}
		
		for(Integer i : triggersDown.keySet()){
			Set<AbstractGizmo> t = triggersDown.get(i);
			if(t != null)
				for(AbstractGizmo g : t)
					if(g == this){
						ret.add(String.format("KeyConnect key %s down %s", i, name));
						break;
					}
		}
		
		return ret;
	}
	
	@Override
	public void paintEffects(IViewDraw v) {}

}
