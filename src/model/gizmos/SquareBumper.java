package model.gizmos;

import java.awt.Color;
import java.rmi.UnexpectedException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import model.tuples.GizmoPhysics;
import view.IViewDraw;

public class SquareBumper extends Bumper {

	public SquareBumper(double x, double y) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		this.c = Color.RED;
	}
	
	public SquareBumper(double x, double y, double w, double h) {
		this.x = Math.floor(x);
		this.y = Math.floor(y);
		this.w = Math.min(Math.floor(w), Math.floor(h));
		this.h = Math.min(Math.floor(w), Math.floor(h));
		this.c = Color.RED;
	}

	@Override
	public void paintGizmo(IViewDraw v) {
		v.drawRectangle(c, x, y, w, h);
	}

	@Override
	public GizmoPhysics asPhysics() {
		return new GizmoPhysics(GizmoPhysics.lsForRectangle(x, y, w, h),
				GizmoPhysics.csForRectangle(x, y, w, h), this, 1.0);
	}

	@Override
	public void setColor(Color c) {
		this.c = c;
	}

	@Override
	public List<String> declaration(Map<AbstractGizmo, String> names)
			throws UnexpectedException {
		String name = names.get(this);
		if (name == null)
			throw new UnexpectedException(ErrorStrings.NAME_WAS_NULL);
		return Arrays.asList(String.format("Square %s %d %d", name, (int) x,
				(int) y));
	}
}
