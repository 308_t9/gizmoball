package model.gizmos;

public class GizmoType {

	Class<?> c;
	String name;

	public GizmoType(Class<?> c, String n) {
		this.c = c;
		this.name = n;
	}

	public Class<?> getType() {
		return c;
	}

	public String getName() {
		return name;
	}

}
