package model.gizmos;

public class ErrorStrings {

	public static final String NAME_WAS_NULL = "Name was null.";

	public static final String NAME_COLLISION = "There was a name collision in your input file.";

	public static final String NUMBER_EXPECTED = "A string which could not be parse into a number was provided where a number was expected.";
}
