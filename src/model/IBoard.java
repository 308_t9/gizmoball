package model;

import java.util.Set;
import java.util.concurrent.BlockingQueue;

import model.gizmos.GizmoType;

public interface IBoard {

	/**
	 * Progress the simulation forward one unit in time.
	 */
	public void tick();

	/**
	 * @return if the square at (x,y) is safe to move to.
	 */
	public boolean squareFree(double x, double y);

	/**
	 * Simulate the machine being shoved by the player.
	 */
	public void shoogle();

	/**
	 * Adds a ball at (x,y)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void addBall(double x, double y) throws ModelConstraintsException;

	/**
	 * Adds a gizmo of type at (x,y)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void addGizmo(GizmoType type, double x, double y)
			throws ModelConstraintsException;

	/**
	 * Adds an elastic gizmo at (x,y)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void addGizmo(GizmoType type, double x, double y, double w, double h)
			throws ModelConstraintsException;

	/**
	 * Access the set of fixed dimension gizmos which the model knows about.
	 */
	public Set<GizmoType> getFixedTypes();

	/**
	 * Access the set of elastic gizmos which the model knows about.
	 */
	public Set<GizmoType> getElasticTypes();

	/**
	 * Delete a gizmo at (x,y)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void deleteGizmo(double x, double y)
			throws ModelConstraintsException;

	/**
	 * Rotate a gizmo at (x,y)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void rotateGizmo(double x, double y)
			throws ModelConstraintsException;

	/**
	 * Move a gizmo at (x,y) to (newX, newY)
	 * 
	 * @throws ModelConstraintsException
	 *             in the event that the constraints of the model would be
	 *             broken by the add.
	 */
	public void moveGizmo(double x, double y, double newX, double newY)
			throws ModelConstraintsException;

	/**
	 * Connect the trigger of the gizmo at (xSource, ySource) to the action of
	 * (xTarget, yTarget)
	 */
	public void connectGizmo(double xSource, double ySource, double xTarget,
			double yTarget);

	/**
	 * Disconnect the trigger of the gizmo at (xSource, ySource) to the action
	 * of (xTarget, yTarget)
	 */
	public void disconnectGizmo(double xSource, double ySource, double xTarget,
			double yTarget);

	/**
	 * Connect the key trigger represented by key to the gizmo at (xTarget,
	 * yTarget)
	 */
	public void connectGizmo(int key, double xTarget, double yTarget);

	/**
	 * Disconnect the key trigger represented by key to the gizmo at (xTarget,
	 * yTarget)
	 */
	public void disconnectGizmo(int key, double xTarget, double yTarget);

	/**
	 * Sets the gravity value of the model. This will be honoured within the
	 * bounds that the model dictates. Negative gravity <strong>is</strong>
	 * supported.
	 */
	public void setGravity(double gravity);

	/**
	 * Sets the friction constants in the model.
	 */
	public void setFrictionConstants(double mu, double mu2);

	/**
	 * Sets the angle of the ball (in radians) where 0 rad is east.
	 */
	public void setBallAngle(double angle);

	/**
	 * Sets the ball speed in L/s. The model may bound the speeds it's willing
	 * to set.
	 */
	public void setBallSpeed(double ballSpeed);

	/**
	 * Set the ball angle and speed as in setBallSpeed and setBallAngle.
	 */
	public void setBallVelocity(double ballSpeed, double theta);

	/**
	 * Sets the key represented by key to down.
	 */
	public void keyDown(int key);

	/**
	 * Sets the key represented by key to up.
	 */
	public void keyUp(int key);

	/**
	 * Starts the simulation running. Start is ignored and therefore safe to
	 * call if the model is started.
	 */
	public void start();

	/**
	 * Stops the simulation. Stop is ignored and therefore safe to call if the
	 * model is stopped.
	 */
	public void stop();

	/**
	 * Gets the queue used to send warning messages to the user. Users of the
	 * interface are <strong>not</strong> required ensure that the queue is
	 * emptied.
	 */
	public BlockingQueue<String> getWarningQueue();

	/**
	 * @return if a gizmo (i.e. an entity which is not the ball) is
	 *         covering the space (x,y).
	 */
	boolean isGizmoAt(double x, double y);
}
