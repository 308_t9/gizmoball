package model;

import java.util.Set;

import model.gizmos.IGizmo;

public interface IBoardView {

	/**
	 * Access the self painting IGizmos.
	 */
	public Set<IGizmo> getGizmos();

}
