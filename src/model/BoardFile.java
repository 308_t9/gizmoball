package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.gizmos.Absorber;
import model.gizmos.Ball;
import model.gizmos.CircleBumper;
import model.gizmos.ErrorStrings;
import model.gizmos.Flipper;
import model.gizmos.FlipperDirection;
import model.gizmos.AbstractGizmo;
import model.gizmos.KeyWords;
import model.gizmos.SquareBumper;
import model.gizmos.TriangleBumper;
import model.gizmos.Walls;
import physics.Vect;

public class BoardFile {

	public static Pattern TRIANGLE = Pattern
			.compile("Triangle ([\\w|\\d]+) (\\d+) (\\d+)");
	public static Pattern SQUARE = Pattern
			.compile("Square ([\\w|\\d]+) (\\d+) (\\d+)");
	public static Pattern CIRCLE = Pattern
			.compile("Circle ([\\w|\\d]+) (\\d+) (\\d+)");
	public static Pattern FLIPPER = Pattern
			.compile("(Left|Right)Flipper ([\\w|\\d]+) (\\d+) (\\d+)");
	public static Pattern KEY_CONNECT = Pattern
			.compile("KeyConnect key (\\d+) (down|up) ([\\d|\\w]+)");
	public static Pattern CONNECT = Pattern
			.compile("Connect ([\\d|\\w]+) ([\\d|\\w]+)");
	public static Pattern ABSORBER = Pattern
			.compile("Absorber ([\\d|\\w]+) (\\d+) (\\d+) (\\d+) (\\d+)");
	public static Pattern BALL = Pattern
			.compile("Ball ([\\d|\\w]+) (\\d+\\.\\d+) (\\d+\\.\\d+) (\\d+\\.\\d+) (\\d+\\.\\d+)");
	public static Pattern DELETE = Pattern.compile("Delete ([\\d|\\w]+)");
	public static Pattern MOVE = Pattern
			.compile("Move ([\\w|\\d]+) ([\\d+\\.\\d+|\\d+]) ([\\d+\\.\\d+|\\d+])");
	public static Pattern ROTATE = Pattern.compile("Rotate ([\\w|\\d]+)");
	public static Pattern GRAVITY = Pattern
			.compile("Gravity ([\\d+\\.\\d+|\\d+])");
	public static Pattern FRICTION = Pattern
			.compile("Friction ([\\d+\\.\\d+|\\d+]) ([\\d+\\.\\d+|\\d+])");

	private BlockingQueue<String> warnings;

	public BoardFile() {
		warnings = null;
	}

	public void save(String filepath, Model model) throws IOException {
		synchronized (model.lock) {
			Map<AbstractGizmo, String> names = new HashMap<>();
			int num = 0;
			for (AbstractGizmo g : model.gizmos) {
				names.put(g, "GIZ" + num++);
			}	
			names.put(model.walls, KeyWords.OUTER_WALLS);
			List<String> saveStrings = new ArrayList<>();
			for (AbstractGizmo g : model.gizmos) {
				saveStrings.addAll(g.declaration(names));
			}
			for (AbstractGizmo g : model.gizmos) {
				saveStrings.addAll(g.connections(names, model.triggersUp, model.triggersDown));
			}

			BufferedWriter bw = new BufferedWriter(new FileWriter(
					new File(filepath)));

			bw.write(String.format("Ball %s %s %s %s %s", "GIZ" + num++, model.ball
					.getX(), model.ball.getY(), model.ball.getVelocity().x(),
					model.ball.getVelocity().y()));
			bw.newLine();
			for (String s : saveStrings) {
				bw.write(s);
				bw.newLine();
			}

			bw.newLine();
			bw.write(String.format("Gravity %s", model.gravity));
			bw.newLine();
			bw.write(String.format("Friction %s %s", model.mu, model.mu2));
			bw.newLine();

			bw.close();

		}
	}

	public Model load(String filepath) throws IOException {
		Set<AbstractGizmo> gizmos = new HashSet<>();
		Ball ball = null;
		Walls walls = new Walls(IBoardConstants.DEFAULT_WIDTH,
				IBoardConstants.DEFAULT_HEIGHT);
		String ballName = "";
		Map<String, AbstractGizmo> names = new HashMap<>();
		names.put(KeyWords.OUTER_WALLS, walls);
		Map<Integer, Set<AbstractGizmo>> triggersUp = new HashMap<Integer, Set<AbstractGizmo>>();
		Map<Integer, Set<AbstractGizmo>> triggersDown = new HashMap<Integer, Set<AbstractGizmo>>();
		double gravity = IBoardConstants.DEFAULT_GRAVITY;
		double mu = IBoardConstants.DEFAULT_MU;
		double mu2 = IBoardConstants.DEFAULT_MU2;
		File f = new File(filepath);
		BufferedReader b = new BufferedReader(new FileReader(f));
		String line;
		while ((line = b.readLine()) != null) {
			try {
				line.trim();
				Matcher m;
				if ((m = ABSORBER.matcher(line)).matches()) {
					int x1 = Integer.parseInt(m.group(2));
					int y1 = Integer.parseInt(m.group(3));
					int x2 = Integer.parseInt(m.group(4));
					int y2 = Integer.parseInt(m.group(5));
					Absorber a = new Absorber(x1, y1, x2 - x1, y2 - y1);
					putName(names, m.group(1), a);
					gizmos.add(a);
				} else if ((m = BALL.matcher(line)).matches()) {
					String ident = m.group(1);
					double x = Double.parseDouble(m.group(2));
					double y = Double.parseDouble(m.group(3));
					double vx = Double.parseDouble(m.group(4));
					double vy = Double.parseDouble(m.group(5));
					ball = new Ball(x, y, new Vect(vx, vy));
					ballName = ident;
					putName(names, ident, null);
				} else if ((m = CIRCLE.matcher(line)).matches()) {
					String ident = m.group(1);
					int x = Integer.parseInt(m.group(2));
					int y = Integer.parseInt(m.group(3));
					CircleBumper c = new CircleBumper(x, y);
					putName(names, ident, c);
					gizmos.add(c);
				} else if ((m = DELETE.matcher(line)).matches()) {
					String ident = m.group(1);
					gizmos.remove(names.remove(ident));
				} else if ((m = FLIPPER.matcher(line)).matches()) {
					FlipperDirection direction = (m.group(1).equals("Left")) ? FlipperDirection.LEFT
							: FlipperDirection.RIGHT;
					String ident = m.group(2);
					int x = Integer.parseInt(m.group(3));
					int y = Integer.parseInt(m.group(4));
					Flipper flipper = new Flipper(x, y, direction);
					putName(names, ident, flipper);
					gizmos.add(flipper);
				} else if ((m = MOVE.matcher(line)).matches()) {
					String ident = m.group(1);
					double x = Double.parseDouble(m.group(2));
					double y = Double.parseDouble(m.group(3));
					AbstractGizmo g = names.get(ident);
					if (g != null)
						g.move(x, y);
					else if (ballName.equals(ident))
						ball.move(x, y);
					else if (warnings != null)
						warnings.put(String.format(
								"Undefined name %s passed to move.", ident));
				} else if ((m = ROTATE.matcher(line)).matches()) {
					String ident = m.group(1);
					AbstractGizmo g = names.get(ident);
					if (g != null)
						g.rotate();
					else if (warnings != null)
						warnings.put(String.format(
								"Undefined name %s passed to rotate.", ident));
				} else if ((m = SQUARE.matcher(line)).matches()) {
					String ident = m.group(1);
					int x = Integer.parseInt(m.group(2));
					int y = Integer.parseInt(m.group(3));
					SquareBumper s = new SquareBumper(x, y);
					putName(names, ident, s);
					gizmos.add(s);
				} else if ((m = TRIANGLE.matcher(line)).matches()) {
					String ident = m.group(1);
					int x = Integer.parseInt(m.group(2));
					int y = Integer.parseInt(m.group(3));
					TriangleBumper t = new TriangleBumper(x, y);
					putName(names, ident, t);
					gizmos.add(t);
				} else if ((m = KEY_CONNECT.matcher(line)).matches()) {
					int key = Integer.parseInt(m.group(1));
					boolean up = m.group(2).equals("up");
					String ident = m.group(3);
					if (up) {
						if (!triggersUp.containsKey(key))
							triggersUp.put(key, new HashSet<AbstractGizmo>());
						AbstractGizmo g = names.get(ident);
						if (g != null)
							triggersUp.get(key).add(names.get(ident));
						else if (warnings != null)
							warnings.put(String.format(
									"Undefined name %s passed to key connect.",
									ident));
					} else {
						if (!triggersDown.containsKey(key))
							triggersDown.put(key, new HashSet<AbstractGizmo>());
						AbstractGizmo g = names.get(ident);
						if (g != null)
							triggersDown.get(key).add(names.get(ident));
						else if (warnings != null)
							warnings.put(String.format(
									"Undefined name %s passed to key connect.",
									ident));
					}
				} else if ((m = CONNECT.matcher(line)).matches()) {
					AbstractGizmo producer = names.get(m.group(1));
					AbstractGizmo consumer = names.get(m.group(2));
					if (producer != null && consumer != null)
						producer.connect(consumer);
					else if (warnings != null) {
						if (producer == null)
							warnings.put(String.format(
									"Undefined name %s passed to connect.",
									m.group(1)));
						else
							warnings.put(String.format(
									"Undefined name %s passed to connect.",
									m.group(2)));
					}
				} else if ((m = GRAVITY.matcher(line)).matches()) {
					gravity = IBoardConstants.bounded(
							IBoardConstants.MAX_GRAVITY,
							IBoardConstants.MIN_GRAVITY,
							Double.parseDouble(m.group(1)));
				} else if ((m = FRICTION.matcher(line)).matches()) {
					mu = IBoardConstants.bounded(IBoardConstants.MAX_MU,
							IBoardConstants.MIN_MU,
							Double.parseDouble(m.group(1)));
					mu2 = IBoardConstants.bounded(IBoardConstants.MAX_MU2,
							IBoardConstants.MIN_MU2,
							Double.parseDouble(m.group(2)));
				}
			} catch (NumberFormatException e) {
				b.close();
				throw new IOException(new MalformedFileException(
						ErrorStrings.NUMBER_EXPECTED));
			} catch (MalformedFileException e) {
				b.close();
				throw new IOException(e);
			} catch (InterruptedException ignored) {
			}
		}

		b.close();
		Model m = new Model(ball, walls, gizmos, triggersUp, triggersDown,
				gravity, mu, mu2);
		m.setWarnings(warnings);
		return m;
	}

	private void putName(Map<String, AbstractGizmo> names, String name,
			AbstractGizmo gizmo) throws MalformedFileException {
		if (names.containsKey(name))
			throw new MalformedFileException(ErrorStrings.NAME_COLLISION);
		names.put(name, gizmo);
	}

	public void setWarnings(BlockingQueue<String> warnings) {
		this.warnings = warnings;
	}
}
