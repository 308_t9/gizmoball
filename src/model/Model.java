package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import model.gizmos.Absorber;
import model.gizmos.Ball;
import model.gizmos.IGizmo;
import model.gizmos.AbstractGizmo;
import model.gizmos.Walls;
import physics.Angle;
import physics.Vect;

public class Model {

	Ball ball;
	Walls walls;
	Set<AbstractGizmo> gizmos;
	final Object lock = new Object();
	AbstractGizmo absorber;
	Map<Integer, Set<AbstractGizmo>> triggersUp;
	Map<Integer, Set<AbstractGizmo>> triggersDown;
	double gravity;
	double mu, mu2;
	double bWidth, bHeight;

	private BlockingQueue<String> warnings;

	public Model(Ball ball, Walls walls, Set<AbstractGizmo> gizmos,
			Map<Integer, Set<AbstractGizmo>> triggersUp,
			Map<Integer, Set<AbstractGizmo>> triggersDown, double gravity,
			double mu, double mu2) {
		this.ball = ball;
		this.walls = walls;
		this.gizmos = gizmos;
		this.triggersUp = triggersUp;
		this.triggersDown = triggersDown;
		this.absorber = null;
		setGravity(gravity);
		setFrictionConstants(mu, mu2);
		this.bWidth = IBoardConstants.DEFAULT_WIDTH;
		this.bHeight = IBoardConstants.DEFAULT_HEIGHT;
	}

	public Model() {
		ball = new Ball(0, 0, Vect.ZERO);
		walls = new Walls(IBoardConstants.DEFAULT_WIDTH,
				IBoardConstants.DEFAULT_HEIGHT);
		gizmos = new HashSet<>();
		triggersUp = new HashMap<Integer, Set<AbstractGizmo>>();
		triggersDown = new HashMap<Integer, Set<AbstractGizmo>>();
		gravity = IBoardConstants.DEFAULT_GRAVITY;
		mu = IBoardConstants.DEFAULT_MU;
		mu2 = IBoardConstants.DEFAULT_MU2;
		this.bWidth = IBoardConstants.DEFAULT_WIDTH;
		this.bHeight = IBoardConstants.DEFAULT_HEIGHT;
	}

	public void setWarnings(BlockingQueue<String> warnings) {
		this.warnings = warnings;
	}

	public void setGravity(double gravity) {
		this.gravity = IBoardConstants.bounded(IBoardConstants.MAX_GRAVITY,
				IBoardConstants.MIN_GRAVITY, gravity);
	}

	public void setFrictionConstants(double mu, double mu2) {
		this.mu = IBoardConstants.bounded(IBoardConstants.MAX_MU,
				IBoardConstants.MIN_MU, mu);
		this.mu2 = IBoardConstants.bounded(IBoardConstants.MAX_MU2,
				IBoardConstants.MIN_MU2, mu2);
	}

	public void setBallVelocity(double size, double theta) {
		Vect velo = new Vect(IBoardConstants.bounded(
				IBoardConstants.MAX_BALL_SPEED, IBoardConstants.MIN_BALL_SPEED,
				size), theta);
		ball.setVelocity(velo);
	}

	public void setBallSpeed(double speed) {
		Vect velo = new Vect(ball.getVelocity().angle(),
				IBoardConstants.bounded(IBoardConstants.MAX_BALL_SPEED,
						IBoardConstants.MIN_BALL_SPEED, speed));
		ball.setVelocity(velo);
	}

	public void setBallAngle(double theta) {
		Vect velo = new Vect(new Angle(theta), ball.getVelocity().length());
		ball.setVelocity(velo);
	}

	public boolean isValid() {
		if (ball == null)
			return false;
		return true;
	}

	public Set<IGizmo> getGizmos() {
		synchronized(lock) {
			Set<IGizmo> ret = new LinkedHashSet<>();
			for (AbstractGizmo g : gizmos)
				ret.add(g);
			if (ball != null)
				ret.add(ball);
			return ret;
		}
	}

	public void keyUp(int key) {
		Set<AbstractGizmo> gs = triggersUp.get(key);
		if (gs != null)
			for (AbstractGizmo g : gs)
				g.keyUp();
	}

	public void keyDown(int key) {
		Set<AbstractGizmo> gs = triggersDown.get(key);
		if (gs != null)
			for (AbstractGizmo g : gs)
				g.keyDown();
	}

	public void keyConnectUp(int key, AbstractGizmo g) {
		if (!triggersUp.containsKey(key))
			triggersUp.put(key, new HashSet<AbstractGizmo>());
		triggersUp.get(key).add(g);
	}

	public void keyConnectDown(int key, AbstractGizmo g) {
		if (!triggersDown.containsKey(key))
			triggersDown.put(key, new HashSet<AbstractGizmo>());
		triggersDown.get(key).add(g);
	}

	public void keyDisconnectDown(int key, AbstractGizmo g) {
		if (triggersDown.containsKey(key)) {
			triggersDown.get(key).remove(g);
		}
	}

	public void keyDisconnectUp(int key, AbstractGizmo g) {
		if (triggersDown.containsKey(key)) {
			triggersUp.get(key).remove(g);
		}
	}

	private boolean isBallAt(double x, double y) {
		int x1 = (int) ball.getX();
		int y1 = (int) ball.getY();
		int x2 = (int) (ball.getX() + 2 * Ball.r);
		int y2 = (int) (ball.getY() + 2 * Ball.r);

		int xRound = (int) x;
		int yRound = (int) y;

		if (x1 == xRound || x2 == xRound) {
			return (y1 == yRound || y2 == yRound);
		}
		return false;
	}

	public boolean isGizmoAt(double x, double y){
		return getGizmoAt(x, y) != null;
	}
	
	private AbstractGizmo getGizmoAt(double x, double y) {
		synchronized(lock) {
			for (AbstractGizmo g : gizmos) {
				if (g.covers(Math.floor(x), Math.floor(y)))
					return g;
			}
		}
		return null;
	}

	public boolean squareFree(double x, double y, AbstractGizmo g){
		if(!isBallAt(x, y)){
			AbstractGizmo onBoard = getGizmoAt(x, y);
			return (onBoard == null || onBoard == g);
		}
		return false;
	}
	
	public boolean squareFree(double x, double y) {
		return squareFree(x, y, null);
	}

	public boolean rectFree(int x, int y, int w, int h, AbstractGizmo g){
		if (x + w <= bWidth && y + h <= bHeight) {
			for (int i = x; i < x + w; i++) {
				for (int j = y; j < y + h; j++) {
					if (!squareFree(i, j, g))
						return false;
				}
			}
		} else {
			warnings.offer("Out of bounds");
			return false;
		}
		return true;	
	}
	
	public boolean rectFree(int x, int y, int w, int h) {
		return rectFree(x,y,w,h,null);
	}

	public void deleteGizmo(double x, double y) {
		AbstractGizmo g = getGizmoAt(x, y);
		if (g instanceof Absorber)
			release();
		synchronized(lock) {
			for (AbstractGizmo source : gizmos)
				source.disconnect(g);

			if(g != null)
				gizmos.remove(g);
			else{
				if(isBallAt(x,y))
					warnings.offer("Can't delete ball");
				else
					warnings.offer("No Gizmo at " +  (int)x + " " + (int)y);

			}
		}
	}

	public void rotateGizmo(double x, double y) {
		AbstractGizmo a = getGizmoAt(x, y);
		if(a != null)
			a.rotate();
		else{
			if(isBallAt(x,y))
				warnings.offer("Can't rotate ball");
			else
				warnings.offer("No Gizmo at " +  (int)x + " " + (int)y);

		}
	}
		

	public void moveGizmo(double x, double y, double newX, double newY) {
		AbstractGizmo g = getGizmoAt(x, y);
		if (g != null && rectFree((int) newX, (int) newY, g.getWidth(), g.getHeight(), g)) {
			g.move(newX, newY);
		} else
			try {
				warnings.put("Cannot move gizmo there!");
			} catch (InterruptedException ignored) {}
	}

	public void connectGizmo(double xSource, double ySource, double xTarget,
			double yTarget) {
		AbstractGizmo source = getGizmoAt(xSource, ySource);
		AbstractGizmo target = getGizmoAt(xTarget, yTarget);
		if (source != null && target != null) {
			source.connect(target);
		}
	}

	public void disconnectGizmo(double xSource, double ySource, double xTarget,
			double yTarget) {
		AbstractGizmo source = getGizmoAt(xSource, ySource);
		AbstractGizmo target = getGizmoAt(xTarget, yTarget);
		if (source != null && target != null) {
			source.disconnect(target);
		}
	}

	public void connectGizmo(int key, double xTarget, double yTarget) {
		AbstractGizmo g = getGizmoAt(xTarget, yTarget);
		if (g != null) {
			Set<AbstractGizmo> up = triggersUp.get(key);
			Set<AbstractGizmo> down = triggersDown.get(key);
			if (up == null)
				triggersUp.put(key, (up = new HashSet<>()));
			up.add(g);
			if (down == null)
				triggersDown.put(key, (down = new HashSet<>()));
			down.add(g);
		}

	}

	public void disconnectGizmo(int key, double xTarget, double yTarget) {
		AbstractGizmo g = getGizmoAt(xTarget, yTarget);
		if (g != null) {
			Set<AbstractGizmo> up = triggersUp.get(key);
			Set<AbstractGizmo> down = triggersDown.get(key);
			if (up != null)
				up.remove(g);
			if (down != null)
				down.remove(g);
		}
	}

	private void release() {
		if (absorber != null)
			absorber.keyDown();
		absorber = null;
	}

	public void addBall(double x, double y) throws ModelConstraintsException {
		AbstractGizmo g;
		release();
		if (squareFree(x, y)) {
			ball = new Ball(x, y, Vect.ZERO);
		} else if ((g = getGizmoAt(x, y)) != null && g instanceof Absorber) {
			absorber = g;
			ball = new Ball(x, y, Vect.ZERO);
			g.collidedWith(ball);
		} else if (!isBallAt(x, y))
			throw new ModelConstraintsException(String.format(
					"Could place ball at %s %s", x, y));
	}
}
