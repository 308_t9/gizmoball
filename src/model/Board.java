package model;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import model.gizmos.Absorber;
import model.gizmos.AbstractGizmo;
import model.gizmos.Ball;
import model.gizmos.CircleBumper;
import model.gizmos.FancyGizmo;
import model.gizmos.Flipper;
import model.gizmos.FlipperDirection;
import model.gizmos.GizmoType;
import model.gizmos.Gizmos;
import model.gizmos.IGizmo;
import model.gizmos.Portal;
import model.gizmos.PortalType;
import model.gizmos.SquareBumper;
import model.gizmos.TriangleBumper;
import model.tuples.Breakable;
import model.tuples.GizmoPhysics;
import physics.Angle;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

public class Board extends Observable implements IBoard, IBoardView, IBoardFile {

	private BoardFile file;
	private Model model;
	private Timer t;
	private double fps = 1.0 / 240.0;
	private Gizmos gizmoTypes;
	private BlockingQueue<String> warnings;

	private Random rand;
	private boolean shoogle;
	private boolean zero;

	public Board() {
		file = new BoardFile();
		model = new Model();
		AbstractGizmo.setModel(model);
		gizmoTypes = new Gizmos();
		warnings = new ArrayBlockingQueue<>(1000);
		file.setWarnings(warnings);
		model.setWarnings(warnings);
		rand = new Random();
		zero = false;
	}

	public class Collision {
		public AbstractGizmo g;
		public double tuc;
		public Vect velo;

		public Collision() {
			tuc = Double.MAX_VALUE;
		}
	}

	@Override
	public Set<IGizmo> getGizmos() {
		return model.getGizmos();
	}

	@Override
	public void shoogle() {
		shoogle = true;
	}

	@Override
	public void tick() {

		Ball b = model.ball;
		double time = fps;

		if (b != null && !b.isStopped()) {

			double frictionScale = (1 - model.mu * time - b.getVelocity()
					.length() * model.mu2 * time);
			b.setVelocity(b.getVelocity().times(frictionScale));
			b.setVelocity(b.getVelocity().plus(
					new Vect(Angle.DEG_90, model.gravity * time)));

			if (shoogle) {
				shoogle = false;
				boolean left = rand.nextBoolean();
				int shoogleFromNormal = rand.nextInt(5);
				double shoogleAngle = left ? 270 - shoogleFromNormal
						: 270 + shoogleFromNormal;
				b.setVelocity(b.getVelocity().plus(
						new Vect(Math.toRadians(shoogleAngle), 5 + rand
								.nextDouble() * 2)));
			}

			Collision col = timeUntilCollision();

			if (col.tuc > time) {
				moveBallForTime(time);
			} else {
				time = col.tuc;
				moveBallForTime(time);
				b.setVelocity(col.velo);
				col.g.collidedWith(b);
			}

		}

		if (time == 0) {
			if (zero)
				time = fps;
			zero = true;
		} else {
			zero = false;
		}

		synchronized (model.lock) {
			for (AbstractGizmo g : model.gizmos)
				g.passTime(time);
		}
		
		this.setChanged();
		this.notifyObservers();

	}

	private void moveBallForTime(double time) {
		Ball b = model.ball;
		double newX = b.getX() + b.getVelocity().x() * time;
		double newY = b.getY() + b.getVelocity().y() * time;
		b.move(newX, newY);
	}

	private Collision timeUntilCollision() {
		Collision c = new Collision();

		double time;
		Set<AbstractGizmo> gizmos = new HashSet<>();
		synchronized (model.lock) {
			gizmos.addAll(model.gizmos);
			gizmos.add(model.walls);	
		}
		for (AbstractGizmo g : gizmos) {
			GizmoPhysics p = g.asPhysics();
			for (LineSegment l : p.ls) {
				time = Geometry.timeUntilWallCollision(l,
						model.ball.asCircle(), model.ball.getVelocity());
				if (time < c.tuc) {
					c.tuc = time;
					c.velo = Geometry.reflectWall(l, model.ball.getVelocity(),
							p.ref);
					c.g = g;
				}
			}
			for (Circle circle : p.cs) {
				time = Geometry.timeUntilCircleCollision(circle,
						model.ball.asCircle(), model.ball.getVelocity());
				if (time < c.tuc) {
					c.tuc = time;
					c.velo = Geometry.reflectCircle(circle.getCenter(),
							model.ball.asVect(), model.ball.getVelocity(),
							p.ref);
					c.g = g;
				}
			}
			for (LineSegment l : p.rls) {
				time = Geometry.timeUntilRotatingWallCollision(l, p.cor,
						p.angVelo, model.ball.asCircle(),
						model.ball.getVelocity());
				if (time < c.tuc) {
					c.tuc = time;
					c.velo = Geometry.reflectRotatingWall(l, p.cor, p.angVelo,
							model.ball.asCircle(), model.ball.getVelocity(),
							p.ref);
					c.g = g;
				}
			}
			for (Circle circle : p.rcs) {
				time = Geometry.timeUntilRotatingCircleCollision(circle, p.cor,
						p.angVelo, model.ball.asCircle(),
						model.ball.getVelocity());
				if (time < c.tuc) {
					c.tuc = time;
					c.velo = Geometry.reflectRotatingCircle(circle, p.cor,
							p.angVelo, model.ball.asCircle(),
							model.ball.getVelocity(), p.ref);
					c.g = g;
				}
			}
		}
		if (c.g instanceof Absorber) {
			model.absorber = c.g;
		}

		return c;

	}

	@Override
	public void addGizmo(GizmoType type, double x, double y) {
		synchronized (model.lock) {
			if (type.getType().equals(Flipper.class)) {
				AbstractGizmo f;
				if (type.getName().equals(Gizmos.FLIPPER_LEFT)) {
					f = new Flipper(x, y, FlipperDirection.LEFT);
					if (model.rectFree((int) x, (int) y, f.getWidth(),
							f.getHeight()))
						model.gizmos.add(f);
					else
						warnings.offer("Could not add Flipper. Overlapping bounding boxes");

				} else if (type.getName().equals(Gizmos.FLIPPER_RIGHT)) {
					f = new Flipper(x, y, FlipperDirection.RIGHT);
					if (model.rectFree((int) x, (int) y, f.getWidth(),
							f.getHeight()))
						model.gizmos.add(f);
					else
						warnings.offer("Could not add Flipper. Overlapping bounding boxes");
				}
			} else if (type.getType().equals(CircleBumper.class)) {
				AbstractGizmo c = new CircleBumper(x, y);
				if (model.rectFree((int) x, (int) y, c.getWidth(), c.getHeight()))
					model.gizmos.add(c);
				else
					warnings.offer("Could not add Circle. Overlapping bounding boxes");
			} else if (type.getType().equals(SquareBumper.class)) {
				AbstractGizmo s = new SquareBumper(x, y);
				if (model.rectFree((int) x, (int) y, s.getWidth(), s.getHeight()))
					model.gizmos.add(s);
				else
					warnings.offer("Could not add Square. Overlapping bounding boxes");
			} else if (type.getType().equals(TriangleBumper.class)) {
				AbstractGizmo t = new TriangleBumper(x, y);
				if (model.rectFree((int) x, (int) y, t.getWidth(), t.getHeight()))
					model.gizmos.add(t);
				else
					warnings.offer("Could not add Triangle. Overlapping bounding boxes");
			} else if (type.getType().equals(FancyGizmo.class)) {
				AbstractGizmo fg = new FancyGizmo(x, y);
				if (model.rectFree((int) x, (int) y, fg.getWidth(), fg.getHeight()))
					model.gizmos.add(fg);
				else
					warnings.offer("Could not add Fancy Gizmo. Overlapping bounding box");
			} else if (type.getType().equals(Portal.class)) {
				AbstractGizmo p;
				if (type.getName().equals(Gizmos.BLUE_PORTAL)) {
					p = new Portal(x, y, PortalType.BLUE);
					if (model.rectFree((int) x, (int) y, p.getWidth(),
							p.getHeight()))
						model.gizmos.add(p);
					else
						warnings.offer("Could not add Portal. Overlapping bounding boxes");

				} else if (type.getName().equals(Gizmos.ORANGE_PORTAL)) {
					p = new Portal(x, y, PortalType.ORANGE);
					if (model.rectFree((int) x, (int) y, p.getWidth(),
							p.getHeight()))
						model.gizmos.add(p);
					else
						warnings.offer("Could not add Portal. Overlapping bounding boxes");
				}
			} else if (type.getType().equals(Breakable.class)) {
				if (type.getName().equals(Gizmos.BREAKABLE_SQUARE)) {
					AbstractGizmo bs = new Breakable(new SquareBumper(x, y));
					if(model.rectFree((int)x, (int)y, bs.getWidth(), bs.getHeight()))
						model.gizmos.add(bs);
					else
						warnings.offer("Could not add Breakable Square. Overlapping bounding boxes");
				} else if (type.getName().equals(Gizmos.BREAKABLE_TRIANGLE)) {
					AbstractGizmo bt = new Breakable(new TriangleBumper(x, y));
					if(model.rectFree((int)x, (int)y, bt.getWidth(), bt.getHeight()))
						model.gizmos.add(bt);
					else
						warnings.offer("Could not add Breakable Triangle. Overlapping bounding boxes");
				} else if (type.getName().equals(Gizmos.BREAKABLE_CIRCLE)) {
					AbstractGizmo bc = new Breakable(new CircleBumper(x, y));
					if(model.rectFree((int)x, (int)y, bc.getWidth(), bc.getHeight()))
						model.gizmos.add(bc);
					else
						warnings.offer("Could not add Breakable Circle. Overlapping bounding boxes");
				}
			}
		}
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void addGizmo(GizmoType type, double x, double y, double w, double h) {
		synchronized (model.lock) {
			if (type.getType().equals(Absorber.class)) {
				AbstractGizmo a = new Absorber(x, y, w, h);
				if (model.rectFree((int) x, (int) y, (int) w, (int) h))
					model.gizmos.add(a);
				else
					warnings.offer("Could not add Absorber. Overlapping bounding boxes");
			} else if (type.getType().equals(SquareBumper.class)) {
				AbstractGizmo ss = new SquareBumper(x, y, w, h);
				if (model.rectFree((int) x, (int) y, (int) w, (int) h))
					model.gizmos.add(ss);
				else
					warnings.offer("Could not add Stretchy Square. Overlapping bounding box");
			} else if (type.getType().equals(CircleBumper.class)) {
				AbstractGizmo sc = new CircleBumper(x, y, w, h);
				if (model.rectFree((int) x, (int) y, (int) w, (int) h))
					model.gizmos.add(sc);
				else
					warnings.offer("Could not add Stretchy Circle. Overlapping bounding box");
			} else if (type.getType().equals(TriangleBumper.class)) {
				AbstractGizmo st = new TriangleBumper(x, y, w, h);
				if (model.rectFree((int) x, (int) y, (int) w, (int) h))
					model.gizmos.add(st);
				else
					warnings.offer("Could not add Stretchy Triangle. Overlapping bounding box");
			} else if (type.getType().equals(Breakable.class)) {
				if (type.getName().equals(Gizmos.STRETCHY_BREAKABLE_SQUARE)) {
					AbstractGizmo sbs = new Breakable(new SquareBumper(x, y, w, h));
					if (model.rectFree((int) x, (int) y, (int) w, (int) h))
						model.gizmos.add(sbs);
					else
						warnings.offer("Could not add Stretchy Breakable Square. Overlapping bounding box");
				} else if (type.getName().equals(Gizmos.STRETCHY_BREAKABLE_TRIANGLE)) {
					AbstractGizmo sbt = new Breakable(new TriangleBumper(x, y, w, h));
					if (model.rectFree((int) x, (int) y, (int) w, (int) h))
						model.gizmos.add(sbt);
					else
						warnings.offer("Could not add Stretchy Breakable Triangle. Overlapping bounding box");
				} else if (type.getName().equals(Gizmos.STRETCHY_BREAKABLE_CIRCLE)) {
					AbstractGizmo sbc = new Breakable(new CircleBumper(x, y, w, h));
					if (model.rectFree((int) x, (int) y, (int) w, (int) h))
						model.gizmos.add(sbc);
					else
						warnings.offer("Could not add Stretchy Breakable Circle. Overlapping bounding box");
				}
			}
		}
		this.setChanged();
		this.notifyObservers();

	}

	@Override
	public Set<GizmoType> getFixedTypes() {
		return gizmoTypes.getFixedTypes();
	}

	@Override
	public Set<GizmoType> getElasticTypes() {
		return gizmoTypes.getElasticTypes();
	}

	@Override
	public void setGravity(double gravity) {
		model.setGravity(gravity);
	}

	@Override
	public void setFrictionConstants(double mu, double mu2) {
		model.setFrictionConstants(mu, mu2);
	}

	@Override
	public void setBallVelocity(double ballSpeed, double theta) {
		model.setBallVelocity(ballSpeed, theta);
	}

	@Override
	public void start() {
		if (t == null) {
			t = new Timer();
			TimerTask task = new TimerTask() {

				@Override
				public void run() {
					tick();
				}

			};
			t.schedule(task, 0, (long) (fps * 1000));
		}
	}

	@Override
	public void stop() {
		if (t != null) {
			t.cancel();
			t.purge();
			t = null;
		}
	}

	@Override
	public void save(String filepath) throws IOException {
		file.save(filepath, model);
	}

	@Override
	public void load(String filepath) throws IOException {
		Model m = file.load(filepath);
		if (m.isValid()) {
			model = m;
			AbstractGizmo.setModel(model);
			this.setChanged();
			this.notifyObservers();
		} else
			warnings.offer("Invalid model. Model doesn't have a ball");
	}

	@Override
	public void keyDown(int key) {
		model.keyDown(key);
	}

	@Override
	public void keyUp(int key) {
		model.keyUp(key);
	}

	@Override
	public boolean squareFree(double x, double y) {
		return model.squareFree(x, y);
	}
	
	@Override
	public boolean isGizmoAt(double x, double y){
		return model.isGizmoAt(x, y);
	}

	@Override
	public void deleteGizmo(double x, double y)
			throws ModelConstraintsException {
		model.deleteGizmo(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void rotateGizmo(double x, double y)
			throws ModelConstraintsException {
		model.rotateGizmo(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void moveGizmo(double x, double y, double newX, double newY)
			throws ModelConstraintsException {
		model.moveGizmo(x, y, newX, newY);
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void connectGizmo(double xSource, double ySource, double xTarget,
			double yTarget) {
		model.connectGizmo(xSource, ySource, xTarget, yTarget);
	}

	@Override
	public void disconnectGizmo(double xSource, double ySource, double xTarget,
			double yTarget) {
		model.disconnectGizmo(xSource, ySource, xTarget, yTarget);
	}

	@Override
	public void connectGizmo(int key, double xTarget, double yTarget) {
		model.connectGizmo(key, xTarget, yTarget);
	}

	@Override
	public void disconnectGizmo(int key, double xTarget, double yTarget) {
		model.disconnectGizmo(key, xTarget, yTarget);
	}

	@Override
	public void addBall(double x, double y) throws ModelConstraintsException {
		model.addBall(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public BlockingQueue<String> getWarningQueue() {
		return warnings;
	}

	@Override
	public void setBallAngle(double angle) {
		model.setBallAngle(angle);
	}

	@Override
	public void setBallSpeed(double ballSpeed) {
		model.setBallSpeed(ballSpeed);
	}

}
