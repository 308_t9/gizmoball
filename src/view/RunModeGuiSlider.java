package view;

import java.awt.Dimension;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import model.IBoard;
import model.IBoardConstants;
import controller.RunModeSlidersController;

@SuppressWarnings("serial")
public class RunModeGuiSlider extends JPanel implements IGuiRunModeSlider {
	private RunModeSlidersController slidersController;

	private JLabel gravLbl;
	private JLabel muLbl;
	private JLabel mu2Lbl;

	private JSlider gravitySlider;
	private JSlider frictionMuSlider;
	private JSlider frictionMu2Slider;

	public RunModeGuiSlider(GuiFrame guiFrame, IBoard model) {
		// Create controllers
		slidersController = new RunModeSlidersController(guiFrame, this, model);

		/* Gravity adjuster */
		gravitySlider = new JSlider(JSlider.HORIZONTAL,
				(int) IBoardConstants.MIN_GRAVITY,
				(int) IBoardConstants.MAX_GRAVITY,
				(int) IBoardConstants.DEFAULT_GRAVITY);

		gravitySlider.setName("Gravity slider");
		gravitySlider.setMajorTickSpacing(10);
		gravitySlider.setMinorTickSpacing(5);
		gravitySlider.setPaintTicks(true);
		gravitySlider.addChangeListener(slidersController);

		gravLbl = new JLabel("Gravity: " + getGravityValue());
		gravLbl.setPreferredSize(new Dimension(150, 20));

		/* Friction Adjustment */
		frictionMuSlider = new JSlider(JSlider.HORIZONTAL,
				(int) (IBoardConstants.MIN_MU),
				(int) (1000 * IBoardConstants.MAX_MU),
				(int) (1000 * IBoardConstants.DEFAULT_MU));
		frictionMuSlider.setName("MU slider");
		frictionMuSlider.setMajorTickSpacing(10);
		frictionMuSlider.setMinorTickSpacing(5);
		frictionMuSlider.setPaintTicks(true);
		frictionMuSlider.addChangeListener(slidersController);

		muLbl = new JLabel("Mu: " + getMuValue());
		muLbl.setPreferredSize(new Dimension(150, 20));

		frictionMu2Slider = new JSlider(JSlider.HORIZONTAL,
				(int) (IBoardConstants.MIN_MU2),
				(int) (1000 * IBoardConstants.MAX_MU2),
				(int) (1000 * IBoardConstants.DEFAULT_MU2));
		frictionMu2Slider.setName("MU2 slider");
		frictionMu2Slider.setMajorTickSpacing(10);
		frictionMu2Slider.setMinorTickSpacing(5);
		frictionMu2Slider.setPaintTicks(true);
		frictionMu2Slider.addChangeListener(slidersController);

		mu2Lbl = new JLabel("Mu2: " + getMu2Value());
		mu2Lbl.setPreferredSize(new Dimension(150, 20));

		add(gravLbl);
		add(gravitySlider);
		add(muLbl);
		add(frictionMuSlider);
		add(mu2Lbl);
		add(frictionMu2Slider);
	}

	public double getGravityValue() {
		return 1.00 * gravitySlider.getValue();
	}

	public void setLabelGravityValue() {
		gravLbl.setText("Gravity: " + getGravityValue());
	}

	public double getMuValue() {
		DecimalFormat df = new DecimalFormat("#.###");
		return Double.valueOf(df.format(0.001 * frictionMuSlider.getValue()));
	}

	public void setLabelMuValue() {
		muLbl.setText("Mu: " + getMuValue());
	}

	public double getMu2Value() {
		DecimalFormat df = new DecimalFormat("#.###");
		return Double.valueOf(df.format(0.001 * frictionMu2Slider.getValue()));
	}

	public void setLabelMu2Value() {
		mu2Lbl.setText("Mu2: " + getMu2Value());
	}
}
