package view;

public class RotationSpecifier {
	public double theta;
	public double tx;
	public double ty;

	public RotationSpecifier(double theta, double tx, double ty) {
		this.theta = theta;
		this.tx = tx;
		this.ty = ty;
	}
}
