package view;

import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.IBoard;
import controller.BuildModeButtonsController;

@SuppressWarnings("serial")
public class GuiBuildMode extends JPanel {
	private String modeName = "Build Mode";

	private BuildModeButtonsController buildModeButtonsController;
	private JButton connectGizButton;

	private IGuiBuildModeSlider guiSlider;

	// GizmoSelectorController buildModeActions;
	public GuiBuildMode(GuiFrame guiFrame, IBoard model, IGuiCanvas canvas) {
		guiFrame.showMessage(MessageType.INFO,
				"Select gizmo, then click on one of the action buttons.");

		// Create controllers
		buildModeButtonsController = new BuildModeButtonsController(guiFrame,
				model, canvas);

		// Create the components for buttonPanel
		JButton moveButton = new JButton("Move");
		moveButton.setPreferredSize(new Dimension(150, 25));
		moveButton.addActionListener(buildModeButtonsController);

		JButton removeButton = new JButton("Remove");
		removeButton.setPreferredSize(new Dimension(150, 25));
		removeButton.addActionListener(buildModeButtonsController);

		JButton rotateButton = new JButton("Rotate");
		rotateButton.setPreferredSize(new Dimension(150, 25));
		rotateButton.addActionListener(buildModeButtonsController);

		connectGizButton = new JButton("Connect Gizmo");
		connectGizButton.setPreferredSize(new Dimension(150, 25));
		connectGizButton.addActionListener(buildModeButtonsController);

		JButton disconnectGizButton = new JButton("Disconnect Gizmo");
		disconnectGizButton.setPreferredSize(new Dimension(150, 25));
		disconnectGizButton.addActionListener(buildModeButtonsController);

		JButton connectKeyButton = new JButton("Connect Key");
		connectKeyButton.setPreferredSize(new Dimension(150, 25));
		connectKeyButton.addActionListener(buildModeButtonsController);

		JButton disconnectKeyButton = new JButton("Disconnect Key");
		disconnectKeyButton.setPreferredSize(new Dimension(150, 25));
		disconnectKeyButton.addActionListener(buildModeButtonsController);

		JPanel sliderPanel = new JPanel(new CardLayout());
		sliderPanel.setPreferredSize(new Dimension(250, 350));
		Border sliderBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0); // top,
																			// left,
																			// bottom,
																			// right
		sliderPanel.setBorder(sliderBorder);
		guiSlider = new BuildModeGuiSlider(guiFrame, model);
		sliderPanel.add((JPanel) guiSlider);

		add(moveButton);
		add(removeButton);
		add(rotateButton);
		add(connectGizButton);
		add(disconnectGizButton);
		add(connectKeyButton);
		add(disconnectKeyButton);
		add(sliderPanel);
		this.setName(modeName);
	}
}
