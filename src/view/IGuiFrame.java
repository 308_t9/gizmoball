package view;

import java.io.IOException;

import javax.swing.JFrame;

public interface IGuiFrame {

	public String loadFile();

	public String savePrompt() throws IOException;

	public boolean promptSave();

	public void showMessage(MessageType mesType, String errorMessage);

	public void setFocus();

	public void switchPanel();

	public JFrame getFrame();
	
	public void switchView();

	public UserAction getActions();

	public void setActions(UserAction actions);
}
