package view;

import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.IBoard;
import controller.RunModeButtonsController;

@SuppressWarnings("serial")
public class GuiRunMode extends JPanel {

	private String modeName = "Run Mode";
	private RunModeButtonsController runModeButtonsController;

	private IGuiRunModeSlider guiSlider;

	public GuiRunMode(GuiFrame guiFrame, IBoard model) {

		// Create controllers
		runModeButtonsController = new RunModeButtonsController(guiFrame, model);

		// Create the components for buttonPanel
		JButton startButton = new JButton("Start");
		startButton.setPreferredSize(new Dimension(150, 25));
		startButton.addActionListener(runModeButtonsController);

		JButton stopButton = new JButton("Stop");
		stopButton.setPreferredSize(new Dimension(150, 25));
		stopButton.addActionListener(runModeButtonsController);

		JButton tickButton = new JButton("Tick");
		tickButton.setPreferredSize(new Dimension(150, 25));
		tickButton.addActionListener(runModeButtonsController);

		JButton shoogleButton = new JButton("Shoogle");
		shoogleButton.setPreferredSize(new Dimension(150, 25));
		shoogleButton.addActionListener(runModeButtonsController);

		JPanel sliderPanel = new JPanel(new CardLayout());
		sliderPanel.setPreferredSize(new Dimension(250, 350));
		Border sliderBorder = BorderFactory.createEmptyBorder(0, 0, 0, 0); // top,
																			// left,
																			// bottom,
																			// right
		sliderPanel.setBorder(sliderBorder);
		guiSlider = new RunModeGuiSlider(guiFrame, model);
		sliderPanel.add((JPanel) guiSlider);

		add(startButton);
		add(stopButton);
		add(tickButton);
		add(shoogleButton);
		add(sliderPanel);
		this.setName(modeName);
	}
}
