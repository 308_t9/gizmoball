package view;

import java.awt.Dimension;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import model.IBoard;
import model.IBoardConstants;
import controller.BuildModeSlidersController;

@SuppressWarnings("serial")
public class BuildModeGuiSlider extends JPanel implements IGuiBuildModeSlider {
	private BuildModeSlidersController slidersController;

	private JLabel gravLbl;
	private JLabel muLbl;
	private JLabel mu2Lbl;
	private JLabel ballSpeedSliderLbl;
	private JLabel ballDirectionSliderLbl;

	private JSlider gravitySlider;
	private JSlider frictionMuSlider;
	private JSlider frictionMu2Slider;
	private JSlider ballSpeedSlider;
	private JSlider ballDirectionSlider;

	public BuildModeGuiSlider(GuiFrame guiFrame, IBoard model) {
		// Create controllers
		slidersController = new BuildModeSlidersController(guiFrame, this,
				model);

		/* Gravity adjuster */
		gravitySlider = new JSlider(JSlider.HORIZONTAL,
				(int) IBoardConstants.MIN_GRAVITY,
				(int) IBoardConstants.MAX_GRAVITY,
				(int) IBoardConstants.DEFAULT_GRAVITY);

		gravitySlider.setName("Gravity slider");
		gravitySlider.setMajorTickSpacing(10);
		gravitySlider.setMinorTickSpacing(5);
		gravitySlider.setPaintTicks(true);
		gravitySlider.addChangeListener(slidersController);

		gravLbl = new JLabel("Gravity: " + getGravityValue());
		gravLbl.setPreferredSize(new Dimension(150, 20));

		/* Friction Adjustment */
		frictionMuSlider = new JSlider(JSlider.HORIZONTAL,
				(int) (IBoardConstants.MIN_MU),
				(int) (1000 * IBoardConstants.MAX_MU),
				(int) (1000 * IBoardConstants.DEFAULT_MU));
		frictionMuSlider.setName("MU slider");
		frictionMuSlider.setMajorTickSpacing(10);
		frictionMuSlider.setMinorTickSpacing(5);
		frictionMuSlider.setPaintTicks(true);
		frictionMuSlider.addChangeListener(slidersController);

		muLbl = new JLabel("Mu: " + getMuValue());
		muLbl.setPreferredSize(new Dimension(150, 20));

		frictionMu2Slider = new JSlider(JSlider.HORIZONTAL,
				(int) (IBoardConstants.MIN_MU2),
				(int) (1000 * IBoardConstants.MAX_MU2),
				(int) (1000 * IBoardConstants.DEFAULT_MU2));
		frictionMu2Slider.setName("MU2 slider");
		frictionMu2Slider.setMajorTickSpacing(10);
		frictionMu2Slider.setMinorTickSpacing(5);
		frictionMu2Slider.setPaintTicks(true);
		frictionMu2Slider.addChangeListener(slidersController);

		mu2Lbl = new JLabel("Mu2: " + getMu2Value());
		mu2Lbl.setPreferredSize(new Dimension(150, 20));

		/* Ball Velocity adjustment */
		ballSpeedSlider = new JSlider(JSlider.HORIZONTAL,
				(int) (IBoardConstants.MIN_BALL_SPEED),
				(int) (IBoardConstants.MAX_BALL_SPEED),
				(int) (IBoardConstants.DEFAULT_BALL_SPEED));
		ballSpeedSlider.setName("Ball Speed Slider");
		ballSpeedSlider.setMajorTickSpacing(20);
		ballSpeedSlider.setMinorTickSpacing(10);
		ballSpeedSlider.setPaintTicks(true);
		ballSpeedSlider.addChangeListener(slidersController);

		ballSpeedSliderLbl = new JLabel("Ball Speed: " + getBallSpeedValue());
		ballSpeedSliderLbl.setPreferredSize(new Dimension(150, 20));

		ballDirectionSlider = new JSlider(JSlider.HORIZONTAL, 0, 360, 0);
		ballDirectionSlider.setName("Ball Direction Slider");
		ballDirectionSlider.setMajorTickSpacing(90);
		ballDirectionSlider.setMinorTickSpacing(10);
		ballDirectionSlider.setPaintTicks(true);
		ballDirectionSlider.addChangeListener(slidersController);

		String s = String.valueOf(Character.toChars(952));
		ballDirectionSliderLbl = new JLabel("Ball Direction " + s + " :"
				+ getBallDirectionValue());
		ballDirectionSliderLbl.setPreferredSize(new Dimension(200, 20));

		add(gravLbl);
		add(gravitySlider);
		add(muLbl);
		add(frictionMuSlider);
		add(mu2Lbl);
		add(frictionMu2Slider);
		add(ballSpeedSliderLbl);
		add(ballSpeedSlider);
		add(ballDirectionSliderLbl);
		add(ballDirectionSlider);
	}

	public double getGravityValue() {
		return 1.00 * gravitySlider.getValue();
	}

	public void setLabelGravityValue() {
		gravLbl.setText("Gravity: " + getGravityValue());
	}

	public double getMuValue() {
		DecimalFormat df = new DecimalFormat("#.###");
		return Double.valueOf(df.format(0.001 * frictionMuSlider.getValue()));
	}

	public void setLabelMuValue() {
		muLbl.setText("Mu: " + getMuValue());
	}

	public double getMu2Value() {
		DecimalFormat df = new DecimalFormat("#.###");
		return Double.valueOf(df.format(0.001 * frictionMu2Slider.getValue()));
	}

	public void setLabelMu2Value() {
		mu2Lbl.setText("Mu2: " + getMu2Value());
	}

	public double getBallSpeedValue() {
		return 1.00 * ballSpeedSlider.getValue();
	}

	public void setLabelBallSpeedValue() {
		ballSpeedSliderLbl.setText("Ball Speed: " + getBallSpeedValue());
	}

	public double getBallDirectionValue() {
		return ballDirectionSlider.getValue();
	}

	public void setLabelBallDirectionValue() {
		String s = String.valueOf(Character.toChars(952));
		ballDirectionSliderLbl.setText("Ball Direction " + s + " :"
				+ getBallDirectionValue());
	}
}