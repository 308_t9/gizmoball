package view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import model.IBoardView;
import model.gizmos.IGizmo;

@SuppressWarnings("serial")
public class GuiCanvas extends Canvas implements Observer, IViewDraw,
		IGuiCanvas {

	private IBoardView board;
	private Set<IGizmo> gizmos;
	private static final int L = 20; // pixels per L

	// Graphics object which represents primary surface
	private Graphics primarySurface;
	// The back buffer image (image we draw into off screen)
	private BufferedImage backBuffer;
	// Dimensions of our board
	private Dimension boardDim;
	private boolean buildMode = true;

	private int sourceX;
	private int sourceY;
	private int rbX;
	private int rbY;
	private int rbW;
	private int rbH;

	public GuiCanvas(IBoardView brd) {
		board = brd;

		gizmos = board.getGizmos();

		boardDim = new Dimension(20 * L, 20 * L);

		// set size background
		this.setSize(boardDim);
		setBackground(Color.BLACK);

		// create and initialise the buffers
		backBuffer = new BufferedImage(boardDim.width, boardDim.height, 1);
		primarySurface = backBuffer.getGraphics();
	}

	@Override
	public void paint(Graphics g) {

		// clear everything on the board
		primarySurface.clearRect(0, 0, boardDim.width, boardDim.height);

		Graphics2D g2d = ((Graphics2D) primarySurface);
		AffineTransform old = g2d.getTransform();
		g2d.scale(L, L);

		// draw all gizmos
		for (IGizmo gizmo : gizmos) {
			gizmo.paintGizmo(this);
		}

		for (IGizmo gizmo : gizmos) {
			gizmo.paintEffects(this);
		}
		g2d.setTransform(old);

		// draw grid lines
		if (buildMode) {
			primarySurface.setColor(Color.WHITE);
			for (int i = 0; i < 20; i++) {
				primarySurface.drawLine(0, L * i, this.getHeight(), L * i);
			}

			for (int i = 0; i < 20; i++) {
				primarySurface.drawLine(L * i, 0, L * i, this.getWidth());
			}
			primarySurface.setColor(Color.BLACK);

			// draw rubber band highlight
			primarySurface.setColor(Color.GREEN);
			primarySurface.drawRect(rbX * L, rbY * L, rbW * L, rbH * L);
			primarySurface.setColor(Color.BLACK);
		}
		// draw to screen;
		g.drawImage(backBuffer, 0, 0, this);

	}

	@Override
	public void drawEllipse(Color c, double x, double y, double w, double h) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		primarySurface.setColor(c);
		Ellipse2D ellipse = new Ellipse2D.Double(x, y, w, h);
		g2d.fill(ellipse);
	}

	@Override
	public void drawEllipse(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs) {

		Graphics2D g2d = (Graphics2D) primarySurface;
		// save previous state
		AffineTransform old = g2d.getTransform();
		primarySurface.setColor(c);
		Ellipse2D ellipse = new Ellipse2D.Double(x, y, w, h);

		reverseRotation(g2d, rs);

		g2d.fill(ellipse);
		g2d.setTransform(old);

	}

	@Override
	public void drawRectangle(Color c, double x, double y, double w, double h) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		Rectangle2D rect = new Rectangle2D.Double(x, y, w, h);

		primarySurface.setColor(c);
		g2d.fill(rect);
	}

	@Override
	public void drawRectangle(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs) {

		Graphics2D g2d = (Graphics2D) primarySurface;
		Rectangle2D rect = new Rectangle2D.Double(x, y, w, h);
		// save previous state
		AffineTransform old = g2d.getTransform();
		primarySurface.setColor(c);

		reverseRotation(g2d, rs);

		g2d.fill(rect);
		g2d.setTransform(old); // restore prev state
	}

	@Override
	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		RoundRectangle2D.Double rrec = new RoundRectangle2D.Double(x, y, w, h,
				arcw, arch);
		

		primarySurface.setColor(c);
		g2d.fill(rrec);

	}

	@Override
	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch, RotationSpecifier... rs) {
		Graphics2D g2d = (Graphics2D) primarySurface;

		RoundRectangle2D.Double rrec = new RoundRectangle2D.Double(x, y, w, h,
				arcw, arch);
		AffineTransform old = g2d.getTransform();
		primarySurface.setColor(c);

		reverseRotation(g2d, rs);

		primarySurface.setColor(c);
		g2d.fill(rrec);
		g2d.setTransform(old); // restore prev state

	}

	public void drawLine(Color c, double x1, double y1, double x2, double y2) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		AffineTransform old = g2d.getTransform();
		g2d.scale(1.0 / L, 1.0 / L);
		Line2D.Double l = new Line2D.Double(x1 * L, y1 * L, x2 * L, y2 * L);
		primarySurface.setColor(c);
		g2d.draw(l);
		g2d.setTransform(old); // restore prev state
	}

	public void drawLine(Color c, double x1, double y1, double x2, double y2,
			RotationSpecifier... rs) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		Line2D l = new Line2D.Double(x1 * L, y1 * L, x2 * L, y2 * L);
		AffineTransform old = g2d.getTransform();
		g2d.scale(1.0 / L, 1.0 / L);
		primarySurface.setColor(c);
		for (int i = rs.length - 1; i >= 0; i--) {
			g2d.rotate(rs[i].theta, rs[i].tx * L, rs[i].ty * L);
		}
		g2d.draw(l);
		g2d.setTransform(old); // restore prev state
	}

	@Override
	public void drawPolygon(Color c, double x, double y, double w, double h, Point2D.Double... points) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		Path2D.Double p = new Path2D.Double();
		primarySurface.setColor(c);

		for (Point2D.Double pt : points) {
			p.lineTo(pt.x, pt.y);
		}

		g2d.fill(p);
	}

	@Override
	public void drawPolygon(Color c, double x, double y, double w, double h, List<RotationSpecifier> rs,
			Double... points) {
		Graphics2D g2d = (Graphics2D) primarySurface;
		Path2D.Double p = new Path2D.Double();
		AffineTransform old = g2d.getTransform();
		primarySurface.setColor(c);

		if (points.length > 0) {
			p.moveTo(points[0].x, points[0].y);
			for (Point2D.Double pt : points) {
				p.lineTo(pt.x, pt.y);
			}

			reverseRotation(g2d, rs);
			g2d.fill(p);
		}

		g2d.setTransform(old);

	}

	private void reverseRotation(Graphics2D g2d, List<RotationSpecifier> rs) {
		RotationSpecifier[] ars = new RotationSpecifier[rs.size()];
		for (int i = 0; i < rs.size(); i++)
			ars[i] = rs.get(i);
		reverseRotation(g2d, ars);
	}

	private void reverseRotation(Graphics2D g2d, RotationSpecifier... rs) {

		for (int i = rs.length - 1; i >= 0; i--) {
			g2d.rotate(rs[i].theta, rs[i].tx, rs[i].ty);
		}

	}

	public void switchMode() {

		if (!buildMode)
			buildMode = true;
		else
			buildMode = false;
		resetRubberBand();
		repaint();
	}

	// Update is called automatically when repaint() is called. Eliminates
	// screen flicker
	public void update(Graphics g) {
		paint(g);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		gizmos = board.getGizmos();
		repaint();
	}

	public void setSource(int x, int y) {
		this.sourceX = x;
		this.sourceY = y;
	}

	public void rubberBandHighlight(int dx, int dy) {
		if (buildMode) {

			if (dy == this.sourceY)
				rbH = 1;
			else
				rbH = Math.abs(dy - this.sourceY) + 1;
			if (dx == this.sourceX)
				rbW = 1;
			else
				rbW = Math.abs(dx - this.sourceX) + 1;

			rbX = Math.min(dx, this.sourceX);
			rbY = Math.min(dy, this.sourceY);

			repaint();
		}

	}

	public void resetRubberBand() {
		this.rbH = 0;
		this.rbW = 0;
	}

	@Override
	public int getL() {
		return L;
	}

	public int getRbX() {
		return rbX;
	}

	public int getRbY() {
		return rbY;
	}

	public int getRbW() {
		return rbW;
	}

	public int getRbH() {
		return rbH;
	}

	@Override
	public int getSourceX() {
		return this.sourceX;
	}

	@Override
	public int getSourceY() {
		return this.sourceY;
	}

	@Override
	public void drawSphere(Color c, double x, double y, double d) {
		drawEllipse(c, x, y, d, d);
	}
}
