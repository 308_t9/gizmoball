package view;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.List;

public interface IViewDraw {
	
	public void drawSphere(Color c, double x, double y, double d);

	public void drawEllipse(Color c, double x, double y, double w, double h);

	public void drawEllipse(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs);

	public void drawRectangle(Color c, double x, double y, double w, double h);

	public void drawRectangle(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs);

	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch);

	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch, RotationSpecifier... rs);

	public void drawLine(Color c, double x1, double y1, double x2, double y2);

	public void drawLine(Color c, double x1, double y1, double x2, double y2,
			RotationSpecifier... rs);

	public void drawPolygon(Color c, double x, double y, double w, double h, Point2D.Double... points);

	public void drawPolygon(Color c, double x, double y, double w, double h, List<RotationSpecifier> rs,
			Point2D.Double... points);
}
