package view;

public interface IGuiBuildModeSlider {

	public void setLabelBallSpeedValue();

	public double getBallSpeedValue();

	public double getMuValue();

	public double getMu2Value();

	public void setLabelMuValue();

	public void setLabelMu2Value();

	public void setLabelGravityValue();

	public double getGravityValue();

	public double getBallDirectionValue();

	public void setLabelBallDirectionValue();
}
