package view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import view.threed.Sketch3D;

import controller.ErrorQueueController;
import controller.GizmoKeyListener;
import controller.CanvasController;
import controller.MagicKeyListener;
import controller.MenuBarController;
import controller.LookAndFeelController;
import controller.RunModeKeyListener;
import model.IBoard;
import model.IBoardFile;
import model.IBoardView;


public class  GuiFrame implements IGuiFrame {
	private JFrame mainFrame;
	private GuiCanvas canvas;
    private Sketch3D sketch3dView;
	private JPanel boardPanel;
	private JPanel buttonPanel;
	private JPanel runModePanel;
	private JPanel buildModePanel; 
	private JPanel gizmoPanel;
	private JPanel gizmoSelectPanel;
	private JTextPane consoleTextPane;
    private enum viewD{threeD,twoD};
    private viewD currentView = viewD.twoD;
	private JMenuItem modeMenuItem;

	private ActionListener menuActions;
	private MagicKeyListener keyListener;
	private MagicKeyListener runmodeKeyListener;
	private LookAndFeelController lookAndFeel;
	private CanvasController canvasController;
	private UserAction action = UserAction.NONE;
	private JPanel gizmoEmptyPanel;
	private JMenuItem viewChoice;
	private JPanel twodPanel;
	private JPanel threedPanel;

	public  GuiFrame(IBoardView boardView, IBoard boardControl, IBoardFile boardFile,Observable o) {
		
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.luna.LunaLookAndFeel");
		} catch (Exception e1) {
			String message = "Cannot load jtattoo Look & Feel.";
			showMessage(MessageType.ERROR, message);
			//e1.printStackTrace();
		}
		
		
		
		// Create controllers
		menuActions = new MenuBarController(this, boardFile, boardControl); 
		
		lookAndFeel = new LookAndFeelController(this);
		new ErrorQueueController(boardControl,this);
		
		// Create the menu bar
		JMenuItem newMenuItem = new JMenuItem("New File");
		newMenuItem.addActionListener(menuActions);

		
		JMenuItem saveMenuItem = new JMenuItem("Save File");
		saveMenuItem.addActionListener(menuActions);

		JMenuItem loadMenuItem = new JMenuItem("Load File");
		loadMenuItem.addActionListener(menuActions);

		modeMenuItem =  new JMenuItem("Run Mode"); 
		modeMenuItem.addActionListener(menuActions);

		 viewChoice =  new JMenuItem("3D");
         viewChoice.addActionListener(menuActions);
		
		JMenuItem exitSystemMenuItem =  new JMenuItem("Exit System");
		exitSystemMenuItem.addActionListener(menuActions);

		Dimension dimension = new Dimension(10, 5);
		JSeparator toolBarSeparator = new JToolBar.Separator(dimension);

		JLabel lfLbl = new JLabel("Look & Feel:    ");

		String[] lookAndFeelStrings = { "Smart", "Hi Fi", "Noire", "Texture", "Aero", "Fast", "Luna" };
		@SuppressWarnings({ "rawtypes", "unchecked" })
		JComboBox lookAndFeelComboBox= new JComboBox(lookAndFeelStrings);
		lookAndFeelComboBox.setMaximumSize(lookAndFeelComboBox.getPreferredSize());
		lookAndFeelComboBox.setSelectedIndex(6);
		lookAndFeelComboBox.addActionListener(lookAndFeel);

		JMenu fileMenu = new JMenu("File");
		JMenu modeMenu = new JMenu("Mode");
		JMenu viewMenu = new JMenu("View");
		JMenu quitMenu = new JMenu("Quit");

		fileMenu.add(newMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(loadMenuItem);
		modeMenu.add(modeMenuItem);
		viewMenu.add(viewChoice);
		quitMenu.add(exitSystemMenuItem);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(modeMenu);
		menuBar.add(viewMenu);
		menuBar.add(quitMenu);
		menuBar.add(toolBarSeparator);
		menuBar.add(lfLbl);
		menuBar.add(lookAndFeelComboBox);

		consoleTextPane = new JTextPane();
		consoleTextPane.setEditable(false);
		consoleTextPane.setAutoscrolls(true);
       
		consoleTextPane.setPreferredSize(new Dimension(200,180));
		consoleTextPane.setFont(new Font("Papyrus", Font.BOLD, 12));
		EmptyBorder eb1 = new EmptyBorder(new Insets(10, 10, 10, 10));
		consoleTextPane.setBorder(eb1);
		consoleTextPane.setMargin(new Insets(5, 5, 5, 5));
		 JScrollPane jsp = new JScrollPane(consoleTextPane);
	        jsp.setAutoscrolls(true);
	        jsp.setPreferredSize(consoleTextPane.getPreferredSize());
		
			runmodeKeyListener = new MagicKeyListener(new RunModeKeyListener( boardControl, this));


		// Create the components for board panel
		canvas = new GuiCanvas(boardView);
		canvasController = new CanvasController(this, canvas, boardControl);
		canvas.addMouseListener((canvasController));
		canvas.addMouseMotionListener(canvasController);
        o.addObserver(canvas);      
		
        
        //JPanel panel = new JPanel();
        sketch3dView= new Sketch3D(o, boardView,runmodeKeyListener);
        sketch3dView.init();
        sketch3dView.noLoop();
	    sketch3dView.setFocusable(true);

        
		viewChoice.setEnabled(false);

        
		
		// Create the console panel
		JPanel consolePanel = new JPanel();
		consolePanel.setPreferredSize(new Dimension(200,200));
		consolePanel.add(jsp);

		// Create the button panel
		buttonPanel = new JPanel(new CardLayout());
		buttonPanel.setPreferredSize(new Dimension(250,500));
		Border buttonPanelBorder = BorderFactory.createEmptyBorder(0,0,20,0); // top, left, bottom, right
		buttonPanel.setBorder(buttonPanelBorder);	
		buildModePanel =  new GuiBuildMode(this, boardControl,canvas);
		runModePanel = new GuiRunMode(this, boardControl);
		buttonPanel.add((JPanel)buildModePanel);
		buttonPanel.add(runModePanel);

		// Create the left outer panel 
		JPanel leftOuterPanel = new JPanel();
		leftOuterPanel.setLayout(new BoxLayout(leftOuterPanel, BoxLayout.Y_AXIS));
		leftOuterPanel.setSize(250, 1000);
		Border leftOuterPanelBorder = BorderFactory.createEmptyBorder(0,10,30,0); // top, left, bottom, right
		leftOuterPanel.setBorder(leftOuterPanelBorder);
		leftOuterPanel.add(consolePanel);
		leftOuterPanel.add(buttonPanel);

		// Create the board panel
		boardPanel = new JPanel(new CardLayout());
		//boardPanel.setSize(400,400);
		Border playingAreaBorder = BorderFactory.createEmptyBorder(50, 0, 0, 0); // top, left, bottom, right
		boardPanel.setBorder(playingAreaBorder);
		
		////////////////View panels/////////////////////
		twodPanel = new JPanel();
		twodPanel.setSize(boardPanel.getSize());
		twodPanel.add(canvas);
		threedPanel = new JPanel();
		threedPanel.setSize(boardPanel.getSize());
		threedPanel.add(sketch3dView);
		boardPanel.add(twodPanel);
		boardPanel.add(threedPanel);
		////////////////End view panels////////////////////////
		
		keyListener = new MagicKeyListener(new GizmoKeyListener( this, boardControl,canvas));
		canvas.addKeyListener(keyListener);
		// Create the gizmo panel
		gizmoPanel =  new JPanel(new CardLayout());
		gizmoPanel.setPreferredSize(new Dimension(250, 500));
		Border  gizmoPanelBorder = BorderFactory.createEmptyBorder(0, 0, 20, 0); // top, left, bottom, right
		gizmoPanel.setBorder( gizmoPanelBorder);
		gizmoSelectPanel = new GuiGizmoSelector(this, boardControl, canvas);
		gizmoEmptyPanel = new JPanel();
		gizmoPanel.add(gizmoSelectPanel);
		gizmoPanel.add(gizmoEmptyPanel);

		// Create the right outer panel
		JPanel rightOuterPanel = new JPanel();
		rightOuterPanel.setLayout(new BoxLayout(rightOuterPanel, BoxLayout.Y_AXIS));
		rightOuterPanel.setPreferredSize(new Dimension(400,700));		
		Border rightOuterPanelBorder = BorderFactory.createEmptyBorder(0,250,50,20); // top, left, bottom, right
		rightOuterPanel.setBorder(rightOuterPanelBorder);	
		  rightOuterPanel.add(boardPanel);
		rightOuterPanel.add(gizmoPanel);
		
		
		
		mainFrame = new JFrame("Gizmoball - Build Mode");
		mainFrame.setPreferredSize(new Dimension(800, 800));
		mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.add(leftOuterPanel);	  
		mainFrame.add(rightOuterPanel);
		mainFrame.setJMenuBar(menuBar);
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null); //  if pack() method is used, setLocationRelativeTo should be used after pack() method call
		mainFrame.setVisible(true);
		try {
			URL imgURL = this.getClass().getResource("/images/gizmoBall.jpg");
			ImageIcon img = new ImageIcon(imgURL);
			mainFrame.setIconImage(img.getImage());
		} catch (Exception e) {
			String message = "Cannot find image icon.";
			showMessage(MessageType.ERROR, message);
			//e.printStackTrace();
		}
	}

	public void switchPanel(){
		// Switching buttons 
		CardLayout cl =  (CardLayout) buttonPanel.getLayout();

		cl.next(buttonPanel);
		//mainFrame.addKeyListener(runmodeKeyListener);
		// Switching frame title and mode menu item 
		String menuItemTxt;
		String title;
		

		if(modeMenuItem.getText().equalsIgnoreCase("Build Mode")){
			title = "Build Mode";
			menuItemTxt = "Run Mode";
			if(currentView.equals(viewD.threeD))
				switchView();
			viewChoice.setEnabled(false);
			canvas.addMouseListener(canvasController);
			canvas.removeKeyListener(runmodeKeyListener);
			canvas.addKeyListener(keyListener);
			showMessage(MessageType.INFO,"Select gizmo, then click on one of the action buttons.");
		} else {
			title = "Run Mode";
			menuItemTxt = "Build Mode";
			viewChoice.setEnabled(true);
			canvas.removeMouseListener(canvasController);
			canvas.addKeyListener(runmodeKeyListener);
			canvas.removeKeyListener(keyListener);
			showMessage(MessageType.INFO,"Press Start to begin");
		}

		mainFrame.setTitle("Gizmoball - "+ title);
		modeMenuItem.setText(menuItemTxt);
		
		// switching gizmo selection
		CardLayout cl2 =  (CardLayout)gizmoPanel.getLayout();
		cl2.next(gizmoPanel);
		
		canvas.switchMode();
		 action = UserAction.NONE;
	}

	// Used to change Look & Feel
	public JFrame getFrame() {
		return mainFrame;
	}

	public void setFocus() {
		if(currentView.equals(viewD.twoD))
				canvas.requestFocus();
		else
			sketch3dView.requestFocus();
		
	
	}

	public void switchView(){
		
		CardLayout cl =  (CardLayout) boardPanel.getLayout();
		cl.next(boardPanel);

		if(currentView.equals(viewD.twoD)){

		    sketch3dView.start();
			viewChoice.setText("2D");
			currentView = viewD.threeD;
		}
		else{
			    sketch3dView.stop();
				viewChoice.setText("3D");
				currentView = viewD.twoD;

		}
	
	}
	
	public boolean promptSave(){
		
		int reply = JOptionPane.showConfirmDialog(null, "Do you want to save current board?", "Save?",  JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION)
		{
		   this.menuActions.actionPerformed(new ActionEvent(this,0,"Save File"));
		   return true;
		}
		else if (reply == JOptionPane.CLOSED_OPTION){
			return false;
		}
		
		 return true;
		
	}
	
	public String loadFile() {
		String filepathToLoad;

		JFileChooser aChooser = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter aFilter = new FileNameExtensionFilter("Gizmoball file (.txt)", "txt");
		aChooser.setFileFilter(aFilter);

		int result = aChooser.showOpenDialog(this.mainFrame);

		if (result == JFileChooser.APPROVE_OPTION) {
			filepathToLoad = aChooser.getSelectedFile().getAbsolutePath();
			return filepathToLoad;
		} else {
			return null;
		}
	}

	public String savePrompt() throws IOException {
		String filepathToSave;

		JFileChooser aChooser = new JFileChooser();
		FileNameExtensionFilter aFilter = new FileNameExtensionFilter("Gizmoball file (.txt)", "txt");
		aChooser.setFileFilter(aFilter);

		int result = aChooser.showSaveDialog(this.mainFrame);

		if (result == JFileChooser.APPROVE_OPTION) {
			if(aChooser.getSelectedFile() == null) {
				aChooser.getSelectedFile().createNewFile();
			}
			filepathToSave = aChooser.getSelectedFile().getAbsolutePath();
			return filepathToSave;
		} else {
			return null;
		}
	}

	public void showMessage(MessageType mesType, String errorMessage) {
		String newline = "\n";

		switch(mesType) {
		case ERROR:
			appendToPane(consoleTextPane, errorMessage + newline, Color.RED);
			break;
		case INFO:
			appendToPane(consoleTextPane, errorMessage + newline, Color.GREEN);
			break;
		case WARNING:
			appendToPane(consoleTextPane, errorMessage + newline, Color.ORANGE);
		default:
			break;
		}
	}
	
	private void appendToPane(JTextPane tp, String msg, Color c) {
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

		Document doc = tp.getDocument();
		
	    try {
			doc.insertString(doc.getLength(), msg, aset);
		} catch (BadLocationException e) {
		}
		
	}
	
	public UserAction getActions() {
		return action;
	}
	
	public void setActions(UserAction actions) {
		this.action = actions;
	}
}
