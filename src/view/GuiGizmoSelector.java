package view;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.IBoard;
import model.gizmos.GizmoType;
import controller.GizmoSelectorController;

@SuppressWarnings("serial")
public class GuiGizmoSelector extends JPanel {

	private GizmoSelectorController gizmoSelectorController;

	public GuiGizmoSelector(IGuiFrame guiFr, IBoard mdl, IGuiCanvas cnv) {
		// Create controllers
		gizmoSelectorController = new GizmoSelectorController(guiFr, mdl, cnv);

		Border border = BorderFactory.createEmptyBorder(30, 0, 50, 0); // top,
																		// left,
																		// bottom,
																		// right
		setBorder(border);

		for (GizmoType g : mdl.getFixedTypes()) {
			JButton button = new JButton(g.getName());
			// button.setPreferredSize(new Dimension(70, 50));
			button.addActionListener(gizmoSelectorController);

			this.add(button);
		}

		for (GizmoType g : mdl.getElasticTypes()) {
			JButton button = new JButton(g.getName());
			// button.setPreferredSize(new Dimension(70, 50));
			button.addActionListener(gizmoSelectorController);

			this.add(button);
		}
		JButton ballButton = new JButton("Ball");
		ballButton.addActionListener(gizmoSelectorController);
		this.add(ballButton);
	}

}
