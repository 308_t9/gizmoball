package view;

public interface IGuiRunModeSlider {

	public double getGravityValue();

	public void setLabelGravityValue();

	public double getMuValue();

	public double getMu2Value();

	public void setLabelMuValue();

	public void setLabelMu2Value();
}
