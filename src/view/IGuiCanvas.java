package view;

public interface IGuiCanvas {

	public void switchMode();

	public void setSource(int x, int y);

	public void rubberBandHighlight(int dx, int dy);

	public int getL();

	public int getRbX();

	public int getRbY();

	public int getRbW();

	public int getRbH();

	public int getSourceX();

	public int getSourceY();

	public void resetRubberBand();
}
