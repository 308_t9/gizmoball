package view.threed;

import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.IBoardView;

public class View3D extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8312056825213582115L;

	Sketch3D sketch3d;
	
	public View3D(Observable observable, IBoardView board) {
		setTitle("3D");
		this.setSize(800, 800);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		//sketch3d = new Sketch3D(observable, board);
		sketch3d.init();
		sketch3d.noLoop();
		panel.add(sketch3d);
		this.add(panel);
		this.setVisible(true);
	}
}
