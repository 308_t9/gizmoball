package view.threed;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Point2D.Double;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import controller.RunModeKeyListener;

import model.IBoardView;
import model.gizmos.IGizmo;
import processing.core.PApplet;
import view.IViewDraw;
import view.RotationSpecifier;

public class Sketch3D extends PApplet  implements Observer, IViewDraw{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4595727135902700010L;
	private IBoardView board;
	private int xAngle, yAngle, zLevel;
	private KeyListener keyListener;
	
	public Sketch3D(Observable observable, IBoardView board,KeyListener k) {
		observable.addObserver(this);
		this.board = board;
		keyListener=k;
	}
	
	@Override
	public void setup() {
		size(400, 400, P3D);
		noSmooth();
		background(0);
		xAngle = 45;
		yAngle = 45;
		zLevel = 2;
	}
		public void keyReleased(){
		      keyListener.keyReleased(new KeyEvent(this, 0, System.currentTimeMillis(), 0, keyCode, key, 0));

			}
	@Override
	public void keyPressed() {
		if (key == CODED) {
			if (keyCode == UP)
				xAngle++;
			else if (keyCode == DOWN)
				xAngle--;
			else if (keyCode == RIGHT)
				yAngle++;
			else if (keyCode == LEFT)
				yAngle--;
			else if (keyCode == KeyEvent.VK_PAGE_UP)
				zLevel++;
			else if (keyCode == KeyEvent.VK_PAGE_DOWN)
				zLevel--;
		}
		redraw();
	      keyListener.keyPressed(new KeyEvent(this, 0, System.currentTimeMillis(), 0, keyCode, key, 0));
	}
	
	@Override
	public void draw() {
		camera(width/2, height/2, (height/2) / tan(PI/6) + zLevel * 100, width/2, height/2, 0, 0, 1, 0);
		translate(width/2, height/2);
		rotateY(radians(yAngle));
		rotateX(radians(xAngle));
		translate(-width/2, -height/2);
		scale(width/20f);
		noStroke();
		background(0);
		lights();
		drawBackground();
		for(IGizmo g : board.getGizmos())
			g.paintGizmo(this);
		for(IGizmo g : board.getGizmos())
			g.paintEffects(this);
		
	}

	@Override
	public void update(Observable o, Object arg) {
		redraw();
	}
	
	public void drawBackground() {
		Color back = new Color(95,95,95);
		fill(back);
		pushMatrix();
		translate(0, 0, -0.51f);
		beginShape();
		rect(0,0,20,20);
		endShape();
		popMatrix();
		
		Color walls = new Color(249, 13, 14);
		fill(walls);
		beginShape();
		vertex(0,0,0);
		vertex(0,0,-0.5f);
		vertex(20,0,-0.5f);
		vertex(20,0,0);
		endShape();
		
		beginShape();
		vertex(20,0,0);
		vertex(20,0,-0.5f);
		vertex(20,20,-0.5f);
		vertex(20,20,0);
		endShape();

		beginShape();
		vertex(0,0,0);
		vertex(0,0,-0.5f);
		vertex(0,20,-0.5f);
		vertex(0,20,0);
		endShape();
		
		beginShape();
		vertex(0,20,0);
		vertex(0,20,-0.5f);
		vertex(20,20,-0.5f);
		vertex(20,20,0);
		endShape();
		
	}

	
	@Override
	public void drawEllipse(Color c, double x, double y, double w, double h) {
		fill(c);
		int sides = 20;
		
		float fx = (float)x;
		float fy = (float)y;
		float fw = ((float)w)/2;
		float fh = ((float)h)/2;
		
		pushMatrix();
		translate(fx+fw, fy+fh);
		
		float angle;
		  float[] xs = new float[sides+1];
		  float[] ys = new float[sides+1];
		 
		  //get the x and z position on a circle for all the sides
		  for(int i=0; i < xs.length; i++){
		    angle = TWO_PI / (sides) * i;
		    xs[i] = sin(angle) * fw;
		    ys[i] = cos(angle) * fw;
		  }
		 
		  //draw the bottom of the cylinder
		  beginShape(TRIANGLE_FAN);
		 
		  vertex(0, 0, -0.5f);
		 
		  for(int i=0; i < xs.length; i++){
		    vertex(xs[i], ys[i], -0.5f);
		  }
		 
		  endShape();
		 
		  //draw the center of the cylinder
		  beginShape(QUAD_STRIP); 
		 
		  for(int i=0; i < xs.length; i++){
		    vertex(xs[i], ys[i], -0.5f);
		    vertex(xs[i], ys[i],  0);
		  }
		 
		  endShape();
		 
		  //draw the top of the cylinder
		  beginShape(TRIANGLE_FAN); 
		 
		  vertex(0, 0, 0);
		 
		  for(int i=0; i < xs.length; i++){
		    vertex(xs[i], ys[i], 0);
		  }
		  endShape();
		  popMatrix();
	}

	@Override
	public void drawEllipse(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs) {
		drawEllipse(c, x, y, w, h);
	}

	@Override
	public void drawRectangle(Color c, double x, double y, double w, double h) {
		fill(c);
		float fx = (float)x;
		float fy = (float)y;
		float fw = (float)w;
		float fh = (float)h;
		
		//Draw bottom
		beginShape();
		vertex(fx, fy, -0.5f);
		vertex(fx+fw, fy, -0.5f);
		vertex(fx+fw, fy+fh, -0.5f);
		vertex(fx, fy+fh, -0.5f);
		endShape();

		//Draw sides
		beginShape(QUAD);
		vertex(fx, fy, 0);
		vertex(fx, fy, -0.5f);
		vertex(fx+fw, fy, -0.5f);
		vertex(fx+fw, fy, 0);
		
		vertex(fx+fw, fy, 0);
		vertex(fx+fw, fy, -0.5f);
		vertex(fx+fw, fy+fh, -0.5f);
		vertex(fx+fw, fy+fh, 0);
		
		vertex(fx+fw, fy+fh, 0);
		vertex(fx+fw, fy+fh, -0.5f);
		vertex(fx, fy+fh, -0.5f);
		vertex(fx, fy+fh, 0);
		
		vertex(fx, fy+fh, 0);
		vertex(fx, fy+fh, -0.5f);
		vertex(fx, fy, -0.5f);
		vertex(fx, fy, 0);
		endShape();
		
		//Draw top
		beginShape();
		vertex(fx, fy, 0);
		vertex(fx+fw, fy, 0);
		vertex(fx+fw, fy+fh, 0);
		vertex(fx, fy+fh, 0);
		endShape();
		
	}

	private void fill(Color c) {
		float r = (float)c.getRed();
		float g = (float)c.getGreen();
		float b = (float)c.getBlue();
		float a = (float)c.getAlpha();
		fill(r, g, b, a);
	}
	
	@Override
	public void drawRectangle(Color c, double x, double y, double w, double h,
			RotationSpecifier... rs) {
		pushMatrix();
		//do the rotation
		reverseRotation(rs);
		//draw the rectangle
		drawRectangle(c, x, y, w, h);
		popMatrix();
	}

	@Override
	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch) {
		fill(c);
		pushMatrix();
		drawRectangle(c, x, y+w/2, w, h-w);
		drawEllipse(c, x, y, w, w);
		drawEllipse(c, x, y+h-w, w, w);
		popMatrix();
	}

	@Override
	public void drawRoundedRectangle(Color c, double x, double y, double w,
			double h, double arcw, double arch, RotationSpecifier... rs) {
		pushMatrix();
		//do the rotation
		reverseRotation(rs);
		//draw the rectangle
		drawRoundedRectangle(c, x, y, w, h, arcw, arch);
		popMatrix();
	}
	
	private void reverseRotation(RotationSpecifier... rs){
		for (int i = rs.length - 1; i >= 0; i--) {
			translate((float)rs[i].tx, (float)rs[i].ty);
			rotateZ((float)rs[i].theta);
			translate(-(float)rs[i].tx, -(float)rs[i].ty);
		}
	}
	
	private void reverseRotation(List<RotationSpecifier> rs) {
		RotationSpecifier[] ars = new RotationSpecifier[rs.size()];
		for (int i = 0; i < rs.size(); i++)
			ars[i] = rs.get(i);
		reverseRotation(ars);
	}

	@Override
	public void drawLine(Color c, double x1, double y1, double x2, double y2) {}
	@Override
	public void drawLine(Color c, double x1, double y1, double x2, double y2,
			RotationSpecifier... rs) {}

	@Override
	public void drawPolygon(Color c, double x, double y, double w, double h, Double... points) {
		fill(c);
		
		pushMatrix();

		//Bottom
		beginShape();
		for(Double p : points) {
			vertex((float)p.x, (float)p.y, -0.5f);
		}
		endShape();
		
		//Sides
		beginShape(QUAD_STRIP);
		if (points.length > 0) {
			for(Double p : points) {
				vertex((float)p.x, (float)p.y, -0.5f);
				vertex((float)p.x, (float)p.y, 0);
			}
			vertex((float)points[0].x, (float)points[0].y, -0.5f);
			vertex((float)points[0].x, (float)points[0].y, 0);
		}
		endShape();
		
		//Top
		beginShape();
		for(Double p : points) {
			vertex((float)p.x, (float)p.y, 0);
		}
		endShape();
		
		
		
		popMatrix();
	}

	@Override
	public void drawPolygon(Color c, double x, double y, double w, double h, List<RotationSpecifier> rs,
			Double... points) {
		pushMatrix();
		reverseRotation(rs);
		drawPolygon(c, x, y, w, h, points);
		popMatrix();
	}

	@Override
	public void drawSphere(Color c, double x, double y, double d) {
		fill(c);
		float fr = (float)d / (float)2;
		float fx = (float)x;// + fr;
		float fy = (float)y;// + fr;
		pushMatrix();
		translate(fx+fr, fy+fr, -fr);
		sphere(fr);
		popMatrix();
	}


	
}
