package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.IBoard;
import model.ModelConstraintsException;
import model.gizmos.GizmoType;
import view.IGuiCanvas;
import view.IGuiFrame;
import view.MessageType;

public class GizmoSelectorController implements ActionListener {
	private IGuiFrame guiFrame;
	private IBoard model;
	private IGuiCanvas canvas;

	public GizmoSelectorController(IGuiFrame guiFr, IBoard mdl, IGuiCanvas cnv) {
		guiFrame = guiFr;
		model = mdl;
		canvas = cnv;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		int x = canvas.getSourceX();
		int y = canvas.getSourceY();

		if (e.getActionCommand().equalsIgnoreCase("Ball")) {
			try {
				model.addBall(x, y);
				String message = "Placed Ball.";
				guiFrame.showMessage(MessageType.INFO, message);

			} catch (ModelConstraintsException e1) {
				guiFrame.showMessage(MessageType.ERROR,
						"You can only place a ball on a free square or an absorber.");
			}
		}

		else {

			for (GizmoType g : model.getFixedTypes()) {

				if (e.getActionCommand().equalsIgnoreCase(g.getName())) {
					
				//	if(canvas.)
					
					
					for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
							.getRbX()) && i < 21; i++) {
						for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
								.getRbY()) && i < 21; j++) {
							if (model.squareFree(i, j))
								try {
									model.addGizmo(g, i, j);
								} catch (ModelConstraintsException e1) {
									

								}
							
						}
					}
				}
			}

			for (GizmoType g : model.getElasticTypes()) {

				if (e.getActionCommand().equalsIgnoreCase(g.getName())) {

					boolean free = true;
					for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
							.getRbX()) && i < 21; i++) {
						for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
								.getRbY()) && i < 21; j++) {
							if (!model.squareFree(i, j))
								free = false;

						}
					}

					if (free) {

						try {
							model.addGizmo(g, canvas.getRbX(), canvas.getRbY(),
									canvas.getRbW(), canvas.getRbH());
					

						} catch (ModelConstraintsException e1) {
							String message = "No gizmo was selected.";
							guiFrame.showMessage(MessageType.ERROR, message);
						} catch (Exception e2) {
							String message = "Unable to add gizmo.";
							guiFrame.showMessage(MessageType.ERROR, message);
						} finally {
							canvas.resetRubberBand();
						}
					}
				}
			}
		}
		canvas.resetRubberBand();
	}
}
