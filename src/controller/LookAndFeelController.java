package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.jtattoo.plaf.aero.AeroLookAndFeel;
import com.jtattoo.plaf.fast.FastLookAndFeel;
import com.jtattoo.plaf.hifi.HiFiLookAndFeel;
import com.jtattoo.plaf.luna.LunaLookAndFeel;
import com.jtattoo.plaf.noire.NoireLookAndFeel;
import com.jtattoo.plaf.smart.SmartLookAndFeel;
import com.jtattoo.plaf.texture.TextureLookAndFeel;

import view.IGuiFrame;
import view.MessageType;

public class LookAndFeelController implements ActionListener {
	private IGuiFrame guiFrame;

	public LookAndFeelController(IGuiFrame guiFr) {
		guiFrame = guiFr;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unchecked")
		JComboBox<String> cb = (JComboBox<String>) e.getSource();

		try {
			Properties props = new Properties();
			props.put("logoString", "T-9");

			// select the Look and Feel
			if (((String) cb.getSelectedItem()).equalsIgnoreCase("Smart")) {

				SmartLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem())
					.equalsIgnoreCase("Hi Fi")) {

				HiFiLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem())
					.equalsIgnoreCase("Noire")) {

				NoireLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem())
					.equalsIgnoreCase("Texture")) {

				TextureLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem()).equalsIgnoreCase("Aero")) {

				AeroLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem()).equalsIgnoreCase("Fast")) {

				FastLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			} else if (((String) cb.getSelectedItem()).equalsIgnoreCase("Luna")) {

				LunaLookAndFeel.setCurrentTheme(props);
				UIManager
						.setLookAndFeel("com.jtattoo.plaf.luna.LunaLookAndFeel");
				SwingUtilities.updateComponentTreeUI(guiFrame.getFrame());
				guiFrame.getFrame().pack();
				guiFrame.getFrame().setVisible(true);

				guiFrame.setFocus();

			}
		} catch (Exception e1) {
			String message = "Cannot load jtattoo Look & Feel.";
			guiFrame.showMessage(MessageType.ERROR, message);
		}
	}
}
