package controller;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.IBoard;
import view.GuiFrame;
import view.IGuiRunModeSlider;

public class RunModeSlidersController implements ChangeListener {
	private IGuiRunModeSlider guiSlider;
	private IBoard brd;
	private GuiFrame guiFrame;

	public RunModeSlidersController(GuiFrame guiFr, IGuiRunModeSlider guiSl,
			IBoard model) {
		guiSlider = guiSl;
		brd = model;
		guiFrame = guiFr;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider slider = (JSlider) e.getSource();
		String sliderName = slider.getName();

		if (sliderName.equalsIgnoreCase("Gravity slider")) {

			guiSlider.setLabelGravityValue();
			brd.setGravity(guiSlider.getGravityValue());
			guiFrame.setFocus();

		} else if ((sliderName.equalsIgnoreCase("MU slider"))
				|| (sliderName.equalsIgnoreCase("MU2 slider"))) {

			guiSlider.setLabelMuValue();
			guiSlider.setLabelMu2Value();
			brd.setFrictionConstants(guiSlider.getMuValue(),
					guiSlider.getMu2Value());
			guiFrame.setFocus();

		}
	}
}
