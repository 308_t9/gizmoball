package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import model.IBoard;
import model.IBoardFile;
import model.ModelConstraintsException;
import view.IGuiFrame;
import view.MessageType;

public class MenuBarController implements ActionListener {
	private IGuiFrame guiFrame;
	private IBoardFile boardFile;
	private IBoard boardControl;

	// Constructor
	public MenuBarController(IGuiFrame guiFr, IBoardFile brdFile,
			IBoard brdControl) {
		guiFrame = guiFr;
		boardFile = brdFile;
		boardControl = brdControl;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equalsIgnoreCase("New File")) {
			boardControl.stop();
			if (guiFrame.promptSave()) {
				for (int i = 0; i < 20; i++) {
					for (int j = 0; j < 20; j++) {
						try {
							if(boardControl.isGizmoAt(i, j))
								boardControl.deleteGizmo(i, j);
						} catch (ModelConstraintsException ignored) {
						}
					}
				}
			}

		}

		else if (e.getActionCommand().equalsIgnoreCase("Load File")) {
			boardControl.stop();
			
			if (guiFrame.promptSave()) {
			
			String filepathToLoad = guiFrame.loadFile();

			// If user changes there mind and closes file chooser,
			// filepathToLoad will be null. We need to make sure it is not null
			// before we send it to model
			if (filepathToLoad != null) {
				try {
					boardFile.load(filepathToLoad);
					String message = "File has been loaded.";
					guiFrame.showMessage(MessageType.INFO, message);
				} catch (IOException e1) {
					String message = "Cannot load file - input/output exception.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}
			}
			}

		} else if (e.getActionCommand().equalsIgnoreCase("Save File")) {

			String filepathToSave = null;

			try {
				filepathToSave = guiFrame.savePrompt();

			} catch (IOException e1) {

				String message = "Failed to create new file.";
				guiFrame.showMessage(MessageType.ERROR, message);
			}

			// If user changed his mind and closed file chooser, filepathToSave
			// will be null. We need to make sure it is not null before we send
			// it to model
			if (filepathToSave != null) {
				try {
					boardFile.save(filepathToSave);
					String message = "Saved the file.";
					guiFrame.showMessage(MessageType.INFO, message);

				} catch (IOException e2) {
					String message = "Unable to save file on your system.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}
			}

		} else if (e.getActionCommand().equalsIgnoreCase("Build Mode")
				|| (e.getActionCommand().equalsIgnoreCase("Run Mode"))) {

			guiFrame.switchPanel();
			boardControl.stop();

		} 
	 else if (e.getActionCommand().equalsIgnoreCase("3D")
			|| (e.getActionCommand().equalsIgnoreCase("2D"))) {

		guiFrame.switchView();
		
		
	}else if (e.getActionCommand().equalsIgnoreCase("Exit System")) {
			if (guiFrame.promptSave()) 
				System.exit(0);
			
		}
	}
}
