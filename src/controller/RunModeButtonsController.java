package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.IBoard;
import view.IGuiFrame;
import view.MessageType;

public class RunModeButtonsController implements ActionListener {
	private IGuiFrame guiFrame;
	private IBoard model;

	// Constructor
	public RunModeButtonsController(IGuiFrame guiFr, IBoard mdl) {
		guiFrame = guiFr;
		model = mdl;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("Start")) {
			model.start();
			String message = "Good luck! You'll need it.";
			guiFrame.showMessage(MessageType.INFO, message);
			guiFrame.setFocus();
		} else if (e.getActionCommand().equalsIgnoreCase("Stop")) {
			model.stop();
			guiFrame.setFocus();
		} else if (e.getActionCommand().equalsIgnoreCase("Tick")) {
			model.tick();
			guiFrame.setFocus();
		} else if (e.getActionCommand().equalsIgnoreCase("Shoogle")) {
			model.shoogle();
			guiFrame.setFocus();
		}
	}
}
