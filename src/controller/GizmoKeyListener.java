package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import view.IGuiCanvas;
import view.IGuiFrame;
import view.MessageType;
import view.UserAction;
import model.IBoard;

public class GizmoKeyListener implements KeyListener {
	private IBoard model;
	private IGuiFrame guiFrame;
	private IGuiCanvas canvas;

	public GizmoKeyListener(IGuiFrame guiFr, IBoard mdl, IGuiCanvas c) {
		model = mdl;
		guiFrame = guiFr;
		canvas = c;
	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (guiFrame.getActions().equals(UserAction.CONNECT_KEY)) {

			for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
					.getRbX()) && i < 21; i++) {
				for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
						.getRbY()) && i < 21; j++) {
					if(model.isGizmoAt(i, j)){

						model.connectGizmo(e.getKeyCode(), i, j);
					}
					

				}
			}
			String message = "Connected key.";
			guiFrame.showMessage(MessageType.INFO, message);
		
	} else if (guiFrame.getActions().equals(UserAction.DISCONNECT_KEY)) {

		for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
				.getRbX()) && i < 21; i++) {
			for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
					.getRbY()) && i < 21; j++) {
				if(model.isGizmoAt(i, j)){

					model.disconnectGizmo(e.getKeyCode(), i, j);
				}
				

			}
		}
		String message = "Disconnected key.";
		guiFrame.showMessage(MessageType.INFO, message);
	

		
	} 
		guiFrame.setActions(UserAction.NONE);
}

@Override
public void keyReleased(KeyEvent e) {

}

@Override
public void keyTyped(KeyEvent arg0) {
}

}
