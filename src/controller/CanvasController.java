package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.SwingUtilities;

import model.IBoard;
import model.IBoardConstants;
import model.ModelConstraintsException;
import view.IGuiCanvas;
import view.IGuiFrame;
import view.MessageType;
import view.UserAction;

public class CanvasController implements MouseListener, MouseMotionListener {
	private IGuiFrame guiFrame;
	private IGuiCanvas canvas;
	private IBoard model;

	private final int L_SCALE;

	public CanvasController(IGuiFrame guiFr, IGuiCanvas cnv, IBoard mdl) {
		guiFrame = guiFr;
		canvas = cnv;
		model = mdl;

		L_SCALE = cnv.getL();

	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX() / L_SCALE;
		int y = e.getY() / L_SCALE;

		if (SwingUtilities.isLeftMouseButton(e)) {

			double firstGizmoX = canvas.getSourceX();
			double firstGizmoY = canvas.getSourceY();
			double secondGizmoX = e.getX() / L_SCALE;
			double secondGizmoY = e.getY() / L_SCALE;

			if (guiFrame.getActions().equals(UserAction.MOVE)) {

				if (!model.isGizmoAt(firstGizmoX, firstGizmoY)) {
					String message = "Please select a gizmo to move";
					guiFrame.showMessage(MessageType.ERROR, message);
				}

				else {

					try {
						model.moveGizmo(firstGizmoX, firstGizmoY, secondGizmoX,
								secondGizmoY);
						String message = "Moved Gizmo";
						guiFrame.showMessage(MessageType.INFO, message);

					} catch (ModelConstraintsException ignored) {}

					guiFrame.setActions(UserAction.NONE);
				}

			} else if (guiFrame.getActions().equals(UserAction.CONNECT_GIZMO)) {

				if (!model.isGizmoAt(firstGizmoX, firstGizmoY)) {
					String message = "Please select a gizmo to connect.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}

				else if (!model.isGizmoAt(secondGizmoX, secondGizmoY)) {
					String message = "Please select a gizmo to connect to.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}

				else {
					try {
						model.connectGizmo(firstGizmoX, firstGizmoY,
								secondGizmoX, secondGizmoY);
						String message = "Connected Gizmo";
						guiFrame.showMessage(MessageType.INFO, message);

					} catch (Exception e1) {
						String message = "Unable to connect gizmo.";
						guiFrame.showMessage(MessageType.ERROR, message);
					}

					guiFrame.setActions(UserAction.NONE);
				}

			} else if (guiFrame.getActions()
					.equals(UserAction.DISCONNECT_GIZMO)) {
				if (!model.isGizmoAt(firstGizmoX, firstGizmoY)) {
					String message = "Please select a gizmo.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}

				else if (!model.isGizmoAt(secondGizmoX, secondGizmoY)) {
					String message = "Please select a gizmo to disconnect.";
					guiFrame.showMessage(MessageType.ERROR, message);
				}

				else {

					try {
						model.disconnectGizmo(firstGizmoX, firstGizmoY,
								secondGizmoX, secondGizmoY);
						String message = "Disconnected Gizmo";
						guiFrame.showMessage(MessageType.INFO, message);

					} catch (Exception e1) {
						String message = "Unable to disconnect gizmo.";
						guiFrame.showMessage(MessageType.INFO, message);
					}

					guiFrame.setActions(UserAction.NONE);
				}
			} else {

				canvas.setSource(x, y);
				canvas.rubberBandHighlight(x, y);
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX() / L_SCALE;
		int y = e.getY() / L_SCALE;

		x = (int) IBoardConstants.bounded(20, 0, x);
		y = (int) IBoardConstants.bounded(20, 0, y);

		canvas.rubberBandHighlight(x, y);
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

}