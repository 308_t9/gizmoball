package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import model.IBoard;
import view.IGuiFrame;


	
	public class RunModeKeyListener implements KeyListener {
		private IBoard model;
		private IGuiFrame guiFrame;
		
		public RunModeKeyListener(IBoard m,IGuiFrame g){
			model = m;
			guiFrame = g;
		}
		
		
		
		
		@Override
		public void keyPressed(KeyEvent e) {
			model.keyDown(e.getKeyCode());

			
		}
		@Override
		public void keyReleased(KeyEvent e) {
			model.keyUp(e.getKeyCode());
			
		}
		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	
}
