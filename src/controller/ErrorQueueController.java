package controller;

import java.util.concurrent.BlockingQueue;

import view.IGuiFrame;
import view.MessageType;
import model.IBoard;

public class ErrorQueueController implements Runnable {

	private IBoard board;
	private IGuiFrame guiFrame;
	private BlockingQueue<String> queue;

	public ErrorQueueController(IBoard b, IGuiFrame g) {

		this.board = b;
		this.guiFrame = g;

		queue = board.getWarningQueue();
		new Thread(this).start();
	}

	@Override
	public void run() {

		while (true) {

			try {
				guiFrame.showMessage(MessageType.WARNING, queue.take());
			} catch (InterruptedException ignored) {
			}

		}

	}

}
