package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.IBoard;
import model.ModelConstraintsException;
import view.IGuiCanvas;
import view.IGuiFrame;
import view.MessageType;
import view.UserAction;

public class BuildModeButtonsController implements ActionListener {
	private IGuiFrame guiFrame;
	private IBoard model;
	private IGuiCanvas canvas;

	public BuildModeButtonsController(IGuiFrame guiFr, IBoard mdl, IGuiCanvas c) {
		guiFrame = guiFr;
		model = mdl;
		canvas = c;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		double x = canvas.getSourceX();
		double y = canvas.getSourceY();

		if (e.getActionCommand().equalsIgnoreCase("Move")) {

			if (!model.isGizmoAt(x, y)) {
				String message = "Select a gizmo.";
				guiFrame.showMessage(MessageType.ERROR, message);
				guiFrame.setActions(UserAction.NONE);
			} else if (canvas.getRbW() > 1 || canvas.getRbH() > 1) {
				String message = "Select only one gizmo.";
				guiFrame.showMessage(MessageType.ERROR, message);
				guiFrame.setActions(UserAction.NONE);
			} else {
				guiFrame.setActions(UserAction.MOVE);
				String message = "Select a free square.";
				guiFrame.showMessage(MessageType.INFO, message);
			}

		} else if (e.getActionCommand().equalsIgnoreCase("Remove")) {
			guiFrame.setActions(UserAction.REMOVE);


			for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
					.getRbX()) && i < 21; i++) {
				for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
						.getRbY()) && i < 21; j++) {
					try {
						model.deleteGizmo(i, j);
					} catch (ModelConstraintsException e1) {

					}

				}
			}
		}


		else if (e.getActionCommand().equalsIgnoreCase("Rotate")) {
			guiFrame.setActions(UserAction.ROTATE);

			for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
					.getRbX()) && i < 21; i++) {
				for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
						.getRbY()) && i < 21; j++) {

					try {
						model.rotateGizmo(i, j);

					} catch (ModelConstraintsException e1) {

					}
				}
			}



		} else if (e.getActionCommand().equalsIgnoreCase("Connect Gizmo")) {

			if (model.isGizmoAt(x, y)){
			guiFrame.setActions(UserAction.CONNECT_GIZMO);
			String message = "Select another gizmo.";
			guiFrame.showMessage(MessageType.INFO, message);
			}
			else
				displayNoSelectedMessage();



		} else if (e.getActionCommand().equalsIgnoreCase("Disconnect Gizmo")) {
			if (model.isGizmoAt(x, y)){
				guiFrame.setActions(UserAction.DISCONNECT_GIZMO);
				String message = "Select another gizmo.";
				guiFrame.showMessage(MessageType.INFO, message);
			}
			else
				displayNoSelectedMessage();

		} else if (e.getActionCommand().equalsIgnoreCase("Connect Key")) {
			boolean noneselected = true;
			for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
					.getRbX()) && i < 21; i++) {
				for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
						.getRbY()) && i < 21; j++) {
					if (model.isGizmoAt(i, j)){
						noneselected = false;
					}

				}
			}
			if(!noneselected){
				guiFrame.setFocus();
				guiFrame.setActions(UserAction.CONNECT_KEY);
				String message = "Select key.";
				guiFrame.showMessage(MessageType.INFO, message);
			}
			else
				displayNoSelectedMessage();


		} else if (e.getActionCommand().equalsIgnoreCase("Disconnect Key")) {
			boolean noneselected = true;
			for (int i = canvas.getRbX(); i < (canvas.getRbW() + canvas
					.getRbX()) && i < 21; i++) {
				for (int j = canvas.getRbY(); j < (canvas.getRbH() + canvas
						.getRbY()) && i < 21; j++) {
					if (model.isGizmoAt(i, j)){
						noneselected = false;
					}

				}
			}
			if(!noneselected){
				guiFrame.setFocus();
				guiFrame.setActions(UserAction.DISCONNECT_KEY);
				String message = "Select key.";
				guiFrame.showMessage(MessageType.INFO, message);
			}
			else
				displayNoSelectedMessage();
		}

	}

	private void displayNoSelectedMessage(){
		String message = "No Gizmo selected.";
		guiFrame.showMessage(MessageType.ERROR, message);
		guiFrame.setActions(UserAction.NONE);

	}

}




