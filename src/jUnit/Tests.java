package jUnit;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Board;
import model.ModelConstraintsException;
import model.gizmos.Absorber;
import model.gizmos.CircleBumper;
import model.gizmos.Flipper;
import model.gizmos.GizmoType;
import model.gizmos.SquareBumper;
import model.gizmos.TriangleBumper;


public class Tests {
      
    @Test(expected=ModelConstraintsException.class)
    public void testAddBall() throws ModelConstraintsException {
    	Board board = new Board();
    	
    	double x = 1.0;
    	double y = 1.0;
    	
        // Check that ball can be successfully added
        board.addBall(x, y);
        
        // and the square is not free
        assertFalse("Square is not free", board.squareFree(x, y));
    }
    
    @Test(expected=ModelConstraintsException.class)
    public void testAddFixedGizmo() throws ModelConstraintsException {
    	Board board = new Board();
    	
    	GizmoType lf = new GizmoType(Flipper.class, "Left Flipper");
    	double x = 1.0;
    	double y = 1.0;
    	
        // Check that fixed gizmo can be successfully added
        board.addGizmo(lf, x, y);
        
        // and the square is not free
        assertFalse("Square is not free", board.squareFree(x, y));
    }
    
    @Test(expected=ModelConstraintsException.class)
    public void testAddElasticGizmo() throws ModelConstraintsException {
    	Board board = new Board();
    	
    	GizmoType abs = new GizmoType(Absorber.class, "Absorber");
    	double x = 1.0;
    	double y = 1.0;
    	double w = 2.0;
    	double h = 2.0;
    	
        // Check that elastic gizmo can be successfully added
        board.addGizmo(abs, x, y, w, h);
        
		for(int i = (int) x; i<(w + x) && i<21; i++){
			for(int j= (int) y; j<(h+y)&& i<21; j++){
		        // and the squares are not free
		        assertFalse("Square is not free", board.squareFree(x, y));		
			}
		}
    }
    
    @Test(expected=ModelConstraintsException.class)
    public void testDeleteGizmo() throws ModelConstraintsException { 
    	Board board = new Board();
    	
    	double x = 1.0;
    	double y = 1.0;
    	
    	GizmoType cb = new GizmoType(CircleBumper.class, "Circle Bumper");
    	board.addGizmo(cb, x, y);
    	
        // Check that gizmo can be successfully deleted
        board.deleteGizmo(x, y);
        
        // and the square is free
        assertTrue("Square is free", board.squareFree(x, y));
    }
    
    @Test(expected=ModelConstraintsException.class)
    public void testMoveGizmo() throws ModelConstraintsException { 
    	Board board = new Board();
    	
    	double x = 1.0;
    	double y = 1.0;
    	double newX = 3.0;
    	double newY = 3.0;
    	
    	GizmoType tb = new GizmoType(TriangleBumper.class, "Triangle Bumper");
    	board.addGizmo(tb, x, y);
    	
        // Check that gizmo can be successfully moved
        board.moveGizmo(x, y, newX, newY);
        
        // and the square is free
        assertTrue("Square is not free", board.squareFree(x, y));
    }
    
    @Test
    public void testGetFixedTypes(){ 
    	Board board = new Board();
    	
    	GizmoType rf = new GizmoType(Flipper.class, "Right Flipper");
    	double x = 1.0;
    	double y = 1.0;
    	
        board.addGizmo(rf, x, y);
    	
        boolean testSize = board.getFixedTypes().size() != 0;
        
        // Test that getFixedTypes() does not return an empty set
        assertTrue("Square is not free", testSize);
    }

    @Test
    public void testGetElasticTypes(){ 
    	Board board = new Board();
    	
    	GizmoType abs = new GizmoType(Absorber.class, "Absorber");
    	double x = 1.0;
    	double y = 1.0;
    	double w = 2.0;
    	double h = 2.0;
    	
        board.addGizmo(abs, x, y, w, h);
    	
        boolean testSize = board.getElasticTypes().size() != 0;
        
        // Test that getElasticTypes() does not return an empty set
        assertTrue("Square is not free", testSize);
    }
    
    @Test
    public void testGetGizmos(){ 
    	Board board = new Board();
    	
    	GizmoType sb = new GizmoType(SquareBumper.class, "Square Bumper");
    	double x = 1.0;
    	double y = 1.0;

    	
    	board.addGizmo(sb, x, y);
    	
    	boolean testSize = board.getGizmos().size() != 0;
        
        // Test that getElasticTypes() does not return an empty set
        assertTrue("Square is not free", testSize);
    }
    
    @Test
    public void testAddTriangleBumper(){ 
    	// We have not tested adding Triangle Bumper gizmo yet,
    	// so this simple test just to make sure we have tested all our gizmos.
    	Board board = new Board();
    	
    	GizmoType tb = new GizmoType(TriangleBumper.class, "Triangle Bumper");
    	double x = 1.0;
    	double y = 1.0;

    	
        // Check that Triangle Bumper can be successfully added
        board.addGizmo(tb, x, y);
        
        // and the square is not free
        assertFalse("Square is not free", board.squareFree(x, y));
    }
}
