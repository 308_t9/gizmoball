Gizmoball, an implementation by 308t9
===

This repository contains an implementation of [Gizmoball](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-170-laboratory-in-software-engineering-fall-2005/projects/6_170_gizmoball.pdf) for [CS308](http://www.strath.ac.uk/cis/localteaching/localug/cs308/) at the [University of Strathclyde](http://www.strath.ac.uk/). It was written by [Uladzimir Barysiuk](https://bitbucket.org/vladbar), [Stewart Gray](https://bitbucket.org/SGray001), [Elliot Iddon](https://bitbucket.org/eiddon) and [Jamie King](https://bitbucket.org/JKing395). It contains many novel features and gizmos not in the original specification.

It was the winner of the Strathclyde Gizmoball competition academic year 2013-14.

Since the end of the project a 3D view has been developed using [P3D](https://www.processing.org/tutorials/p3d/) a library which binds to OpenGL.

It's available under the terms in license.txt

Extra Features
==

* Stretchy and breakable versions of the standard bumpers.
* Fancy Gizmo (windmill).
* Portals.
* Themeable look and feel to the interface.
* 3D View.
* Live adjustments to Gravity and Friction during simulation.
* Build mode usability features: multi add, multi rotate, multi remove, multi key bind.
* Particle effects on bumper collision.

Screenshots
==

![Standard View](https://dl.dropboxusercontent.com/u/15771268/gb_screens/run_standard.png)
![Standard View 3D](https://dl.dropboxusercontent.com/u/15771268/gb_screens/run_standard_3d.png)
![Extra Gizmos](https://dl.dropboxusercontent.com/u/15771268/gb_screens/extra_gizmos.png)
![Look and Feel](https://dl.dropboxusercontent.com/u/15771268/gb_screens/look_and_feel.png)
![Particle Effects](https://dl.dropboxusercontent.com/u/15771268/gb_screens/particle_effects.png)

